import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';

class TrackingModel {

  var id;
  List<String> users;
  User nursePro;
  User senderPro;

  GeoPoint nurse;
  GeoPoint sender;

  var description;
  var pay;
  var hireId;
  Job job;

  bool isInstant = false;


  TrackingModel({this.id, this.users, this.nursePro, this.senderPro, this.nurse, this.sender,
    this.description, this.pay, this.hireId, this.job,
    this.isInstant = false,
  });

}