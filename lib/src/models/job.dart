import 'package:nurses_app/src/models/user.dart';

class Job {
  var id;
  var userId;
  var title;
  var pay;
  var hours;
  var description;
  var applied;
  int status;
  String createdAt;
  String updatedAt;
  User user;

  Job(
      {this.id,
      this.userId,
      this.title,
      this.pay,
      this.hours,
      this.description,
      this.status,
      this.createdAt,
      this.updatedAt, this.user});

  Job.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    title = json['title'];
    pay = json['pay'];
    hours = json['hours'];
    description = json['description'];
    status = json['status'];
    applied = json['applied'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['title'] = this.title;
    data['pay'] = this.pay;
    data['hours'] = this.hours;
    data['description'] = this.description;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}