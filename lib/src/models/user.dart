import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/models/payment_method.dart';
import 'package:nurses_app/src/models/qualification.dart';
import 'package:nurses_app/src/models/specification.dart';

class User {


  var categoryName;

  bool isNewPhoto = false;
  var newPhotoIdentifier = "";
  var newPhotoName = "";


  double rating;
  DateTime createdOn;

  var speciality;



  var latitude;
  var longitude;
  var address;

////////////////////////////////
  var id;
  var name="";
  var email;
  var password;

  var emailVerifiedAt;
  var apiToken;
  var type;
  var genre; //2 For Nurse, 1 for Hospital/User
  var catId;
  var lat;
  var long;
  var image = "";
  var licenseNo;
  var location;
  var phone;
  var distance;
  String firebaseToken;
  String updatedAt;
  String createdAt;
  List<Qualification> qualifications;
  List<Specification> specifications;

  Category category;
  bool verified;

  var balance;
  PaymentMethod payMethod;

  User({this.apiToken,
    this.categoryName,
        this.name,
        this.email,
        this.password,
        this.type,
    this.balance,
        this.createdOn,
        this.category,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.phone,
    this.payMethod,
        this.image, this.speciality,
        this.location, this.latitude, this.longitude,
        this.address, this.rating});

 User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    verified = json['verified'] ?? true;
    emailVerifiedAt = json['email_verified_at'];
    type = json['type'];
    genre = json['genre'];
    catId = json['cat_id'];
    apiToken = json['api_token'] ?? null;
    lat = json['lat'];
    long = json['long'];
    licenseNo = json['license_no'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    phone = json['phone'];
    licenseNo = json['license_no'];
    image = json['image'];
    firebaseToken = json['firebase_token'];
    distance = json['distance'] ?? "";
    balance = json['balance'];
    this.payMethod = PaymentMethod.fromJson(json);
    if (json['qualifications'] != null) {
      qualifications = new List<Qualification>();
      json['qualifications'].forEach((v) {
        qualifications.add(new Qualification.fromJson(v));
      });
    }
    if (json['specifications'] != null) {
      specifications = new List<Specification>();
      json['specifications'].forEach((v) {
        specifications.add(new Specification.fromJson(v));
      });
    }
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    this.id != null ? data['id'] = this.id : null;
    this.name != null ? data['name'] = this.name : null;
    this.email != null ? data['email'] = this.email : null;
    this.password != null ? data['password'] = this.password : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    data['type'] = "1";
    data['genre'] = 2;
    this.catId != null ? data['cat_id'] = this.catId : null;
    this.apiToken != null ? data['api_token'] = this.apiToken : null;
    this.licenseNo != null ? data['license_no'] = this.licenseNo : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6) : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    this.image != null ? data['image'] = image.toString() ?? "1" : null;
    this.firebaseToken != null ? data['firebase_token'] = this.firebaseToken : null;
    if (this.qualifications != null) {
      data['qualifications'] =
          this.qualifications.map((v) => v.toJson()).toList();
    }
    if (this.specifications != null) {
      data['specifications'] =
          this.specifications.map((v) => v.toJson()).toList();
    }
    if(this.category != null) {
      data['category'] = category.toJson();
    }
    return data;
  }

  Map<String, String> toMap() {
    final Map<String, String> data = new Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    this.name != null ? data['name'] = this.name.toString() : null;
    this.email != null ? data['email'] = this.email.toString() : null;
    this.password != null ? data['password'] = this.password.toString() : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    data['type'] = "1";
    data['genre'] = "2";
    this.catId != null ? data['cat_id'] = this.catId.toString() : null;
    this.apiToken != null ? data['api_token'] = this.apiToken.toString() : null;
    this.licenseNo != null ? data['license_no'] = this.licenseNo.toString() : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6)  : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    this.firebaseToken != null ? data['firebase_token'] = this.firebaseToken.toString() : null;
    // if (this.qualifications != null) {
    //   data['qualifications'] =
    //       this.qualifications.map((v) => v.toJson()).toList();
    // }
    // if (this.specifications != null) {
    //   data['specifications'] =
    //       this.specifications.map((v) => v.toJson()).toList();
    // }
    return data;
  }



  Map<String, dynamic> toSaveJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    this.id != null ? data['id'] = this.id : null;
    this.name != null ? data['name'] = this.name : null;
    this.email != null ? data['email'] = this.email : null;
    this.password != null ? data['password'] = this.password : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    data['type'] = "1";
    data['genre'] = 2;
    this.catId != null ? data['cat_id'] = this.catId : null;
    this.apiToken != null ? data['api_token'] = this.apiToken : null;
    this.licenseNo != null ? data['license_no'] = this.licenseNo : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6) : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    this.image != null ? data['image'] = image.toString() ?? "1" : null;
    this.firebaseToken != null ? data['firebase_token'] = this.firebaseToken : null;
    if(this.payMethod != null && this.payMethod.cardNumber != null) {
      data['card_name'] = this.payMethod.cardName.toString();
      data['card_number'] = this.payMethod.cardNumber.toString();
      data['card_cvc'] = this.payMethod.cvc.toString();
      data['card_expiry'] = this.payMethod.expiry.toString();
    }
    if (this.qualifications != null) {
      data['qualifications'] =
          this.qualifications.map((v) => v.toJson()).toList();
    }
    if (this.specifications != null) {
      data['specifications'] =
          this.specifications.map((v) => v.toJson()).toList();
    }
    if(this.category != null) {
      data['category'] = category.toJson();
    }
    return data;
  }

}