import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';

class JobTrack {

  var id;
  var hireId;
  var pay;
  var status; //inProgress/Done

  DateTime time;
  var description;

  Job job;
  User nurse;
  User sender;
  List<String> users;

  bool isInstant = false;

  JobTrack({this.id, this.hireId, this.pay,
      this.status, this.time,
    this.isInstant = false,
      this.description, this.job, this.nurse,
      this.sender, this.users});

}