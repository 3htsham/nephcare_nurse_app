class Qualification {
  var id;
  var degree;
  var year;
  var institute;

  Qualification({this.id, this.degree, this.year, this.institute});

  Qualification.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? null;
    degree = json['degree'];
    institute = json['institude'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['degree'] = this.degree;
    data['institude'] = this.institute;
    data['year'] = this.year;
    return data;
  }

}