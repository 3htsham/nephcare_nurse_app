

import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/models/chat.dart';
import 'package:nurses_app/src/models/hire_request.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/job_track.dart';
import 'package:nurses_app/src/models/user.dart';

class RouteArgument {

  int currentTab;
  String title;

  Category category;

  User currentUser;
  User user;

  Job job;

  Chat chat;

  HireRequest request;

  bool getUserDetails = false;

  String trackingReqId;

  bool isAddingPaymentFromJob = false;

  var onGoingJobId;
  JobTrack jobToTrack;
  var test;

  RouteArgument({this.currentTab, 
    this.title,
    this.request,
    this.category,
    this.user,
    this.getUserDetails = false,
    this.currentUser,
    this.job,
    this.chat,
    this.trackingReqId,
    this.isAddingPaymentFromJob = false,
    this.onGoingJobId,
    this.jobToTrack,
  });

}