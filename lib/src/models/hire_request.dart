import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';

class HireRequest {

  var id;
  var description;
  Job job;
  User sender;
  User nurse;
  List<String> users;
  var status;

  DateTime time;
  bool isScheduled = false;
  DateTime schedule;

  double pay;

  String category;
  String categoryName;
  String senderId;
  bool isInstant = false;

  HireRequest({this.id,
    this.description, this.pay, this.time, this.job, this.sender,
    this.schedule, this.nurse, this.users, this.isScheduled = false,
    this.status,
    this.isInstant = false,
    this.category,
    this.categoryName,
    this.senderId,
  });

}