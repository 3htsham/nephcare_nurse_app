import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class NeuContainer extends StatelessWidget {

  final double borderRadius;
  final double width;
  double height;
  Widget child;

  NeuContainer({
    Key key,
    @required this.child,
    this.width = 100,
    this.height,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final outerShadow = BoxDecoration(
      borderRadius: BorderRadius.circular(borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
    );

    return height != null 
    ? Container(
      width: width,
      height: height,
      decoration: outerShadow,
      child: Align(
        alignment: Alignment.center,
        child: child == null ? Container(width: 0, height: 0,) : child,
      ),
    ) 
    : Container(
      width: width,
      decoration: outerShadow,
      child: Align(
        alignment: Alignment.center,
        child: child == null ? Container(width: 0, height: 0,) : child,
      ),
    );
  }
}

