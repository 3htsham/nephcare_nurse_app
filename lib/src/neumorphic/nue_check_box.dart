import 'package:nurses_app/src/neumorphic/concave_decoration.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class NeuBtnCheckBox extends StatefulWidget {

  final VoidCallback onPressed;
  final double borderRadius;
  final double width;
  final double height;
  bool state;
  Widget child;

  NeuBtnCheckBox({
    Key key,
    this.width = 50,
    this.height = 50,
    this.child,
    this.state,
    @required this.onPressed,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  _NeuCheckBoxState createState() => _NeuCheckBoxState();
}

class _NeuCheckBoxState extends State<NeuBtnCheckBox> {
  bool _isPressed = false;

  @override
  void initState() {
    _isPressed = widget.state;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final outerShadow = BoxDecoration(
      // border: Border.all(color: neumorphicTheme.borderColor),
      borderRadius: BorderRadius.circular(widget.borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
      // gradient: config.Colors().innerGradient()
    );

    return InkWell(
      onTap: widget.onPressed,
      child: AnimatedContainer(
        width: widget.width,
        height: widget.height,
        margin: EdgeInsets.only(bottom: 5, right: 5),
        duration: Duration(milliseconds: 50),
        decoration: widget.state ? outerShadow : ConcaveDecoration(
            shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(widget.borderRadius ?? 100)),
            colors: [config.Colors().shadowLight, config.Colors().shadowDark],
            depth: 6
        ),
        child: Align(
          alignment: Alignment.center,
          child: widget.child == null ? Container(width: 0, height: 0,) : widget.child,
        ),
      ),
    );
  }
}
