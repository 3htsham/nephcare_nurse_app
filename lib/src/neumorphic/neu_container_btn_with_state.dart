import 'package:nurses_app/src/neumorphic/concave_decoration.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class NeuBtnContainerWithState extends StatefulWidget {

  final bool isChosen;
  final bool isPill;
  final VoidCallback onPressed;
  final double borderRadius;
  final double width;
  final double height;
  var state;
  Widget child;

  NeuBtnContainerWithState({
    Key key,
    this.isPill = false,
    this.width = 100,
    this.height = 50,
    this.child,
    this.state,
    @required this.onPressed,
    this.isChosen = false,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  _NeuBtnContainerWithStateState createState() => _NeuBtnContainerWithStateState();
}

class _NeuBtnContainerWithStateState extends State<NeuBtnContainerWithState> {
  bool _isPressed = false;

  @override
  void didUpdateWidget(NeuBtnContainerWithState oldWidget) {
    if (oldWidget.isChosen != widget.isChosen) {
      setState(() => _isPressed = widget.isChosen);
    }
    super.didUpdateWidget(oldWidget);
  }

  void _onPointerDown(PointerDownEvent event) {
    setState(() => _isPressed = true);
    Future.delayed(Duration(milliseconds: 10), (){
      widget.onPressed();
    });
  }

  void _onPointerUp(PointerUpEvent event) {
    setState(() => _isPressed = widget.isChosen);
  }

  @override
  Widget build(BuildContext context) {

    final outerShadow = BoxDecoration(
      // border: Border.all(color: neumorphicTheme.borderColor),
      borderRadius: BorderRadius.circular(widget.borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
      // gradient: config.Colors().innerGradient()
    );

    return Listener(
      onPointerDown: _onPointerDown,
      onPointerUp: _onPointerUp,
      child: AnimatedContainer(
        width: widget.width,
        height: widget.height,
        margin: EdgeInsets.only(bottom: 5, right: 5),
        duration: Duration(milliseconds: 50),
        decoration: widget.state ? outerShadow : BoxDecoration(
          color: Colors.transparent
        ),
        child: Align(
          alignment: Alignment.center,
          child: widget.child == null ? Container(width: 0, height: 0,) : widget.child,
        ),
      ),
    );
  }
}
