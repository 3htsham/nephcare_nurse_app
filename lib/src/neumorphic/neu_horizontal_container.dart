import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class NeuHorizontalContainer extends StatelessWidget {

  final double borderRadius;
  double width;
  final double height;
  Widget child;

  NeuHorizontalContainer({
    Key key,
    @required this.child,
    this.width,
    this.height = 100,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final outerShadow = BoxDecoration(
      borderRadius: BorderRadius.circular(borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
    );

    return width != null 
    ? Container(
      width: width,
      height: height,
      decoration: outerShadow,
      child: Align(
        alignment: Alignment.center,
        child: child == null ? Container(width: 0, height: 0,) : child,
      ),
    ) 
    : Container(
      height: height,
      decoration: outerShadow,
      child: Align(
        alignment: Alignment.center,
        child: child == null ? Container(width: 0, height: 0,) : child,
      ),
    );
  }
}

