import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/models/qualification.dart';
import 'package:nurses_app/src/models/specification.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class QualAndSpecsController extends ControllerMVC {

  User currentUser;
  bool isLoading = false;

  List<Qualification> qualifications = <Qualification>[];
  Qualification qualification = Qualification();
  List<String> years = <String>[];
  TextEditingController yearController;

  List<Specification> specifications = <Specification>[];
  Specification specification = Specification();

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> qualificationFormKey;
  GlobalKey<FormState> specificationFormKey;

  QualAndSpecsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    qualificationFormKey = new GlobalKey<FormState>();
    specificationFormKey = new GlobalKey<FormState>();
    yearController = TextEditingController();
  }

  void updateQualificationSpecs() async {
    setState((){isLoading = true;});
    if(await isInternetConnection()) {
      userRepo.updateUserQualificationSpecs(this.qualifications, this.specifications).then((value) async {
        setState((){isLoading = false;});
        if(value != null) {
          Navigator.of(context).pop(true);
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Unable to update now"),));
        }
      }, onError: (e){
        setState((){isLoading = false;});
        print(e);
      });
    } else {
      setState((){isLoading = true;});
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection"),));
    }
  }


  addYears(){
    for(int i=0; i<50; i++) {
      DateTime date = DateTime.now();
      years.add((date.year-i).toString());
    }
  }
  selectYear(String year) {
    setState((){
      yearController.text = year;
    });
  }
  addUserQualification(){
    if(qualificationFormKey.currentState.validate()) {
      qualificationFormKey.currentState.save();
      setState((){
        qualifications.add(qualification);
        qualification = Qualification();
        qualificationFormKey.currentState.reset();
        yearController.text = "";
      });
    }
  }
  removeQualification(int index) {
    setState(() {
      qualifications.removeAt(index);
    });
  }

  //Experience or Specification
  addUserExperience(){
    if(specificationFormKey.currentState.validate()) {
      specificationFormKey.currentState.save();
      setState((){
        specifications.add(specification);
        specification = Specification();
        specificationFormKey.currentState.reset();
      });
    }
  }
  removeUserExperience(int index){
    setState(() {
      specifications.removeAt(index);
    });
  }

}