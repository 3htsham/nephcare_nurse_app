import 'dart:math';
import 'package:image_picker/image_picker.dart';
import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart';

import '';
import 'package:nurses_app/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/category_repository.dart' as catRepo;

class ProfileController extends ControllerMVC {

  User currentUser;
  bool isLoading = false;
  bool showDialog = false;
  bool isNewImage = false;
  List<Category> categories = <Category>[];
  Category selectedCategory;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> editFormKey;

  ProfileController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    editFormKey = new GlobalKey<FormState>();
  }

  void getCategories() async {
    setState((){
      isLoading = true;
    });
    if(await isInternetConnection()) {
      setState((){
        categories.clear();
      });
      Stream<Category> stream = await catRepo.getCategories();
      stream.listen((_category) {
        if(_category != null) {
          setState((){
            this.categories.add(_category);
          });
        }
      },
          onError: (e){
            print(e);
            setState((){
              isLoading = false;
            });
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error getting Categories list")));
          },
          onDone: (){
            setState((){
              isLoading = false;
              categories.forEach((element) {
                if(element.id.toString() == this.currentUser.catId.toString()) {
                  selectedCategory = element;
                }
              });
            });
          });
    } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }

  void updateUserNow() async {
    if(editFormKey.currentState.validate()) {
      editFormKey.currentState.save();
      setState(() {
        isLoading = true;
      });
      currentUser.catId = this.selectedCategory.id;
      Stream<User> stream = await userRepo.updateUserProfile(currentUser);
      stream.listen((_user) {
        if (_user != null) {
          Navigator.of(context).pop(true);
        }
      }, onError: (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
        scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text("Error updating profile")));
      }, onDone: () {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  showPhotoDialog() {
    setState((){
      showDialog = true;
    });
  }
  closeDialog(){
    setState((){
      showDialog = false;
    });
  }

  void getCameraImage() async {
    closeDialog();
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      var _identifier = pickedFile.path;
      var _name = "Image_${_rand.nextInt(100000)}";
      setState((){
        this.currentUser.isNewPhoto = true;
        this.currentUser.newPhotoIdentifier = _identifier;
        this.currentUser.newPhotoName = _name;
      });
    }
  }
  void getGalleryImage() async {
    closeDialog();
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      var _identifier = pickedFile.path;
      var _name = "Image_${_rand.nextInt(100000)}";
      setState((){
        this.currentUser.isNewPhoto = true;
        this.currentUser.newPhotoIdentifier = _identifier;
        this.currentUser.newPhotoName = _name;
      });
    }
  }


}