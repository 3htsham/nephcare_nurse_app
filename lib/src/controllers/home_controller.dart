import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/my_firestore.dart';
import 'package:nurses_app/src/models/hire_request.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/job_track.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/job_repository.dart' as jobRepo;

class HomeController extends ControllerMVC {

  User currentUser;
  List<Job> jobs = <Job>[];

  var trackId;

  GlobalKey<ScaffoldState> scaffoldKey;

  List<HireRequest> requests = <HireRequest>[];
  List<JobTrack> jobTracks = <JobTrack>[];
  List<HireRequest> instantRequests = <HireRequest>[];

  HomeController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  void getJobs() async {
    Stream<Job> stream = await jobRepo.getJobs();
    stream.listen((_job) {
      setState((){
        jobs.add(_job);
      });
    }, 
    onError: (e){
      print(e);
    }, 
    onDone: (){});
  }


  getMyHireRequests() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        this.currentUser = value;
      });
      MyFirestore.getHireRequests(this.currentUser.id.toString()).listen((_snaps){
        requests.clear();
        _snaps.docs.forEach((_doc) {
          Map<String, dynamic> data = _doc.data();

          HireRequest _req = HireRequest(id: data['id'], description: data['description'], status: data['status'],);

          if(data['job'] != null) {
            Job _job = Job(id: data['job']['id'], title: data['job']['title'], description: data['job']['description']);
            _req.job = _job;
          }

          User _nurse = User(id: data['nurse']['id'], name: data['nurse']['name'],
              image: data['nurse']['image']);

          _req.nurse = _nurse;

          User _sender = User(id: data['sender']['id'], name: data['sender']['name'],
              image: data['sender']['image'], type: data['sender']['type']);

          _req.sender = _sender;
          _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

          setState((){
            this.requests.add(_req);
          });

        });
      },
          onError: (e){
            print(e);
          },
          onDone: (){}
      );

      MyFirestore.getMyTrackingRoomsStream(this.currentUser.id.toString()).listen((_snap) {
        if(_snap.docs.isNotEmpty) {
          _snap.docs.forEach((_doc) {
            setState((){
              this.trackId = _doc.id;
            });
          });
        }
        else {
          setState((){
            this.trackId = null;
          });
        }
      },
          onError: (e){
            print(e);
          },
          onDone: (){});
    });

  }

  getMyOnGoingJobs() async {
    userRepo.getCurrentUser().then((value) {

      MyFirestore.getMyJobTrackingRooms(this.currentUser.id.toString()).listen((_snaps){
        _snaps.docs.forEach((_doc) {
          Map<String, dynamic> data = _doc.data();

          JobTrack _track = JobTrack(id: data['id'], hireId: data['id'],
            description: data['description'], pay: data['pay'], status: data['status'],
              time:  DateTime.fromMillisecondsSinceEpoch(data['time']));

          _track.nurse = User(id: data['nursepro']['id'], name: data['nursepro']['name'],
              image: data['nursepro']['image']);

          _track.sender = User(id: data['senderpro']['id'], name: data['senderpro']['name'],
              image: data['senderpro']['image']);

          if(data['job'] != null) {
            Job _job = Job(id: data['job']['id'], title: data['job']['title'], description: data['job']['description']);
            _track.job = _job;
          }

          setState((){
            this.jobTracks.add(_track);
          });

        });
      },
          onError: (e){
            print(e);
          },
          onDone: (){}
      );
    });
  }


  //INSTANT REQUESTS
  getMyInstantHireRequests() async {
    print('Getting');
    MyFirestore.getInstantHireRequestsByCategory(this.currentUser?.category?.id.toString()).listen((_snaps){
      setState((){
        instantRequests.clear();
      });
      _snaps.docs.forEach((_doc) {
        Map<String, dynamic> data = _doc.data();

        HireRequest _req = HireRequest(
          id: data['id'],
          category: data['category'],
          categoryName: data['categoryName'],
          status: data['status'],
          isInstant: true,
        );

        User _sender = User(id: data['sender']['id'], name: data['sender']['name'],
            image: data['sender']['image'], type: data['sender']['type']);

        GeoPoint _senderGeoPoint = (data['sender']['location'] as GeoPoint);
        if(_senderGeoPoint != null) {
          LatLng _senderLoc = LatLng(
              _senderGeoPoint?.latitude, _senderGeoPoint?.longitude);

          _sender.lat = _senderLoc.latitude;
          _sender.long = _senderLoc.longitude;
        }

        _req.sender = _sender;
        _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

        if(this.instantRequests.where((element) => element.id == _req.id).isEmpty) {
          setState(() {
            this.instantRequests.add(_req);
          });
        }

      });
    },
        onError: (e){
          print(e);
        },
        onDone: (){}
    );
  }
  

}