import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as repo;
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/models/user.dart';

class VerificationController extends ControllerMVC {
  User currentUser;

  bool isLoading = false;
  bool isVerified = false;
  TextEditingController controller;

  GlobalKey<ScaffoldState> scaffoldKey;

  VerificationController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.controller = TextEditingController();
  }


  verify() async {
    if(controller.text == null || controller.text.length <1) {
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Enter verification code please")));
    } else {
      setState((){isLoading = true;});
      repo.verifyEmail(this.currentUser.id.toString(), controller.text.toString()).then((value) {
        if(value != null) {
          setState((){isLoading = true; isVerified = true;});
          //Register Success
          Future.delayed(Duration(milliseconds: 200), (){
            Navigator.of(context).pushNamedAndRemoveUntil("/Qualification",
                    (Route<dynamic> route) => false,
                arguments: RouteArgument(currentUser: value));
          });
        } else {
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Invalid Verification Code")));
        }
      },
          onError: (e){
            setState((){isLoading = false;});
        print(e);
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
      });
    }
  }

}