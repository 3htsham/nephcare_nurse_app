import 'dart:typed_data';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/my_firestore.dart';
import 'package:nurses_app/config/strings.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/tracking_model.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:location/location.dart' as l;
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/notifications_repository.dart' as notiRepo;


class TrackingController extends ControllerMVC {

  String trackingId;

  TrackingModel trackingDetails;
  String destination="";

  GoogleMapController controller;
  BitmapDescriptor customIcon;
  bool isMapCreated = false;
  String mapStyle;
  List<Marker> allMarkers = [];
  List<Polyline> polylines = <Polyline>[];
  List<LatLng> routeCoords;

  User sender;
  bool reached = false;
  bool isFirstTime = true;

  MarkerId myId = MarkerId("My location");

  GoogleMapPolyline mapPolyline = GoogleMapPolyline(apiKey: DIRECTIONS_API_KEY);

  GlobalKey<ScaffoldState> scaffoldKey;

  TrackingController(){
    scaffoldKey = GlobalKey<ScaffoldState>();
  }

  void getUserDetails() async {
    userRepo.getUserProfile(this.trackingDetails.senderPro.id.toString()).then((_user) {
      if (_user != null) {
        setState(() {
          this.sender = _user;
        });
        if(this.isFirstTime) {
          setState((){ this.isFirstTime = false;});
          moveCameraToDestination();
        }
      }
    }, onError: (e) {
      print(e);
    });
  }

  getTracking() async {

    MyFirestore.getTrackingRoom(this.trackingId).listen((_docSnap) {
      if(_docSnap.exists) {

        Map<String, dynamic> data = _docSnap.data();

        TrackingModel _trkDetls = TrackingModel(
          id: data['id'].toString(),
          nursePro: User(id: data['nursepro']['id'], name: data['nursepro']['name'], image: data['nursepro']['img'], categoryName: data['nursepro']['categoryName']),
          senderPro: User(id: data['senderpro']['id'], name: data['senderpro']['name'], image: data['senderpro']['img'], type: data['senderpro']['type']),
          sender: data['sender'] as GeoPoint,
          nurse: data['nurse'] as GeoPoint,

          description: data["description"],
          pay: data['pay'],
          hireId: data['hireId'],
          isInstant: data['isInstant'] ?? false,
          job: data['job'] != null ? Job(id: data['job']['id'],
              hours: data['job']['hours'],
              description: data['job']['description'],
              title: data['job']['title']) : null
        );

        setState((){
          this.trackingDetails = _trkDetls;
        });

        LatLng _dest = LatLng(_trkDetls.sender.latitude, _trkDetls.sender.longitude);
        getAddressAndLocation(_dest);

        getUserDetails();
      } else {
        Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
      }
    },
        onError: (e){
      print(e);
        }, onDone: (){
        });
  }

  updateMyLocation() async {
    l.LocationData myLoc = await l.Location.instance.getLocation();

    var _lat = myLoc.latitude;
    var _long = myLoc.longitude;
    this.trackingDetails.nursePro.lat = myLoc.latitude;
    this.trackingDetails.nursePro.long = myLoc.longitude;
    GeoPoint _point = GeoPoint(_lat, _long);
    addMarkerNurse(myLoc);

    MyFirestore.updateTrackingLocation(this.trackingId, _point);

    l.Location.instance.onLocationChanged.listen((_data) {
      var _lat = _data.latitude;
      var _long = _data.longitude;
      GeoPoint _point = GeoPoint(_lat, _long);

      addMarkerNurse(_data);

      MyFirestore.updateTrackingLocation(this.trackingId, _point);

    },
        onError: (e){
      print(e);
        });
  }

  notifyReached() async {
    String title = "${this.trackingDetails.nursePro.name} is here";
    String body = "${this.trackingDetails.nursePro.name} has just reached at your location.";
    notiRepo.sendNotification(body, title, this.sender.firebaseToken);
    setState((){reached = true;});
  }

  endTracking() async {
    if(this.trackingDetails != null) {
      MyFirestore.finishTracking(this.trackingId);

      /*
          Map will be like
          id = hireId
          users = [ 1 (senderId), 2 (nurseId)]
          nursepro = { id: 1, name: nurseName, img: http://www.imagelink.com, categoryName: SomeCategory}
          senderpro = { id: 1, name: userOrHospitalName, img: http://www.imagelink.com, type: user/Hospital}
         description = "Job description here"
         job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
         pay: hirePay
          status: inProgress/Done
         time: 0123456 ///When Started
         hireId: Id
      */

      Map<String, dynamic> _jobTrackMap = Map<String, dynamic>();
      _jobTrackMap['id'] = this.trackingDetails.hireId;
      _jobTrackMap['users'] = [
        this.trackingDetails.nursePro.id.toString(),
        this.trackingDetails.senderPro.id.toString()
      ];

      Map<String, String> _nurse = Map<String, String>();
      _nurse['id'] = this.trackingDetails.nursePro.id.toString();
      _nurse['name'] = this.trackingDetails.nursePro.name.toString();
      _nurse['img'] = this.trackingDetails.nursePro.image;
      _nurse['categoryName'] = this.trackingDetails.nursePro.category != null
          ? this.trackingDetails.nursePro.category.name
          : "";

      Map<String, String> _sender = Map<String, String>();
      _sender['id'] = this.trackingDetails.senderPro.id.toString();
      _sender['name'] = this.trackingDetails.senderPro.name.toString();
      _sender['img'] = this.trackingDetails.senderPro.image;
      _sender['type'] = this.trackingDetails.senderPro.type.toString();


      _jobTrackMap['nursepro'] = _nurse;
      _jobTrackMap['senderpro'] = _sender;

      _jobTrackMap['description'] = this.trackingDetails.description;
      _jobTrackMap['pay'] = this.trackingDetails.pay;
      _jobTrackMap['hireId'] = this.trackingDetails.hireId;
      _jobTrackMap['isInstant'] = this.trackingDetails.isInstant;

      if(this.trackingDetails.job != null) {
        Map<String, dynamic> jobMap = Map<String, dynamic>();
        jobMap['id'] = this.trackingDetails.job.id;
        jobMap['title'] = this.trackingDetails.job.title;
        jobMap['description'] = this.trackingDetails.job.description;
        jobMap['hours'] = this.trackingDetails.job.hours;

        _jobTrackMap['job'] = jobMap;
      }

      MyFirestore.createJobTrackingRoom(_jobTrackMap,  this.trackingDetails.hireId);




      String title = "${this.trackingDetails.nursePro.name} is here";
      String body = "${this.trackingDetails.nursePro
          .name} has just ended the live tracking.";
      notiRepo.sendNotification(body, title, this.sender.firebaseToken);
      Navigator.of(context).pushNamedAndRemoveUntil(
          "/Pages", (Route<dynamic> route) => false, arguments: 2);
    }
  }


  getRouteCoordinates() async {
    routeCoords = await mapPolyline.getCoordinatesWithLocation(
        origin: LatLng(this.trackingDetails.nurse.latitude, this.trackingDetails.nurse.longitude),
        destination: LatLng(this.trackingDetails.sender.latitude, this.trackingDetails.sender.longitude),
        mode: RouteMode.driving
    );

    polylines.clear();
    setState((){
      polylines.add(Polyline(
          polylineId: PolylineId("Route1"),
          color: Colors.blue,
          startCap: Cap.roundCap,
          endCap: Cap.buttCap,
          width: 5,
          points: routeCoords
      ));
    });
  }

  moveCamera(l.LocationData _loc) {
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: this.trackingDetails == null
            ? LatLng(40.71552837085483, -73.98514218628407)
            : LatLng(double.parse(_loc.latitude.toString()),
            double.parse(_loc.longitude.toString())),
        zoom: 17.0,)));

    getRouteCoordinates();
  }

  moveCameraToDestination() async {
    if(this.sender != null) {
      addMarker(sender);
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: sender.lat == null
              ? LatLng(40.71552837085483, -73.98514218628407)
              : LatLng(double.parse(
              sender.lat.toString()),
              double.parse(sender.long.toString())),
          zoom: 14.0,)));
    }
  }

  void addMarker(User _user){
    getBytesFromAsset('assets/img/mappin.png', 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
        setState((){
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: MarkerId(_user.name),
            draggable: false,
            infoWindow:
            InfoWindow(title: _user.name, snippet: "is here"),
            position: LatLng(double.parse(_user.lat.toString()),
                double.parse(_user.long.toString())),));
        });
      // moveCamera();
    });
  }

  void addMarkerNurse(l.LocationData _loc) {

    String marker = 'assets/img/mymappin.png';

    if(this.trackingDetails.nursePro.categoryName.toString().toLowerCase() == "babysitter") {
      marker = 'assets/img/bmappin.png';
    }

    getBytesFromAsset(marker, 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
      bool isUpdated = false;
      allMarkers.forEach((_mrkr) {
        if(_mrkr.markerId == myId) {
          isUpdated = true;
          setState((){
            _mrkr = Marker(
              icon: customIcon,
              markerId: myId,
              draggable: false,
              infoWindow:
              InfoWindow(title: "You're here", snippet: "current location"),
              position: LatLng(double.parse(_loc.latitude.toString()),
                  double.parse(_loc.longitude.toString())),);
          });
        }
      });
      if(!isUpdated) {
        setState((){
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: myId,
            draggable: false,
            infoWindow:
            InfoWindow(title: "You're here", snippet: "current location"),
            position: LatLng(double.parse(_loc.latitude.toString()),
                double.parse(_loc.longitude.toString())),));
        });
      }
      moveCamera(_loc);
    });
  }

  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }

  getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
      updateMyLocation();
    } else {
      l.Location().requestPermission().then((value) {
        if (value == l.PermissionStatus.denied) {
          getLocationPermission();
        } else {
          updateMyLocation();
        }
      });
    }
  }
  initLocationController(){
    setCustomMapPin();
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  void setCustomMapPin() async {
    getBytesFromAsset('assets/img/mappin.png', 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
    });
  }
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }

  void getAddressAndLocation(LatLng position) async {
    double latitude = position.latitude;
    double longitude = position.longitude;
    Coordinates coordinates = Coordinates(latitude, longitude);

    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var address = addresses.first.featureName +", " + addresses.first.locality + " " + addresses.first.countryName;
    print("Getting hospital/user location: " + address);
    setState((){
      this.destination = address;
    });
  }

}