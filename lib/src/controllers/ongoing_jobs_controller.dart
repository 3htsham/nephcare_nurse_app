import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/my_firestore.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/job_track.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/job_repository.dart' as jobRepo;

class OnGoingJobController extends ControllerMVC {

  User currentUser;
  bool isLoading = false;
  JobTrack jobToTrack;
  List<JobTrack> jobTracks = <JobTrack>[];

  GlobalKey<ScaffoldState> scaffoldKey;

  OnGoingJobController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  getMyOnGoingJobs() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        currentUser = value;
      });

      MyFirestore.getMyJobTrackingRooms(this.currentUser.id.toString()).listen((_snaps){
        _snaps.docs.forEach((_doc) {
          Map<String, dynamic> data = _doc.data();

          JobTrack _track = JobTrack(id: data['id'], hireId: data['id'],
              isInstant: data['isInstant'] ?? false,
              description: data['description'], pay: data['pay'], status: data['status'],
              time:  DateTime.fromMillisecondsSinceEpoch(data['time']));

          _track.nurse = User(id: data['nursepro']['id'], name: data['nursepro']['name'],
              image: data['nursepro']['img']);

          _track.sender = User(id: data['senderpro']['id'], name: data['senderpro']['name'],
              image: data['senderpro']['img']);

          if(data['job'] != null) {
            Job _job = Job(id: data['job']['id'], title: data['job']['title'],
                description: data['job']['description']);
            _track.job = _job;
          }

          setState((){
            this.jobTracks.add(_track);
          });

        });
      },
          onError: (e){
            print(e);
          },
          onDone: (){}
      );
    });
  }

  getJobToTrackDetails() async {

    MyFirestore.getJobTrackingRoom(this.jobToTrack.id).listen((_docSnap) {
      if(_docSnap.exists) {

        Map<String, dynamic> data = _docSnap.data();

        JobTrack _track = JobTrack(id: data['id'], hireId: data['id'],
            isInstant: data['isInstant'] ?? false,
            description: data['description'], pay: data['pay'], status: data['status'],
            time:  DateTime.fromMillisecondsSinceEpoch(data['time']));

        _track.nurse = User(id: data['nursepro']['id'], name: data['nursepro']['name'],
            image: data['nursepro']['image']);

        _track.sender = User(id: data['senderpro']['id'], name: data['senderpro']['name'],
            image: data['senderpro']['image']);

        if(data['job'] != null) {
          Job _job = Job(id: data['job']['id'], title: data['job']['title'],
              description: data['job']['description'], pay: data['pay']);
          _track.job = _job;
        }

        setState((){
          this.jobToTrack = _track;
        });


      } else {
        // Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
      }
    },
        onError: (e){
          print(e);
        }, onDone: (){
        });
  }

}