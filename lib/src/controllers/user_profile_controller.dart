import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/job_repository.dart' as jobRepo;
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class UserProfileController extends ControllerMVC {

  User currentUser;
  User user;

  List<Job> jobs = <Job>[];

  GoogleMapController controller;
  String mapStyle;
  List<Marker> allMarkers = [];

  GlobalKey<ScaffoldState> scaffoldKey;

  UserProfileController() {
    scaffoldKey = GlobalKey<ScaffoldState>();
  }

  void getUserJobs() async {
    Stream<Job> stream = await jobRepo.getUserJobs(this.user.id);
    stream.listen((_job) {
      setState((){
        _job.user = this.user;
        jobs.add(_job);
      });
    },
        onError: (e){
          print(e);
        },
        onDone: (){});
  }

  void getCurrentUser() async {
    userRepo.getCurrentUser().then((_user) {
      setState((){
        this.currentUser = _user;
      });
    });
  }

  void getUserDetails() async {
    userRepo.getUserProfile(this.user.id.toString()).then((_user) {
      if(_user != null) {
        setState((){
          this.user = _user;
        });
        moveCamera();
      }
    }, onError: (e){
      print(e);
    });
  }




  initLocationController(){
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }
  moveCamera() {
    if (user.long != null && user.lat != null) {
      getBytesFromAsset('assets/img/mappin.png', 60).then((value) {
        var customIcon = BitmapDescriptor.fromBytes(value);
        setState(() {
          allMarkers.clear();
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: MarkerId('${user.id}'),
            draggable: false,
            infoWindow: InfoWindow(title: user.name, snippet: "is here"),
            position: LatLng(double.parse(user.lat.toString()),
                double.parse(user.long.toString())),
          ));
        });
      });
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(double.parse(user.lat.toString()),
              double.parse(user.long.toString())),
          zoom: 13.0,
          bearing: 45.0,
          tilt: 45.0)));
    }
  }
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }

}