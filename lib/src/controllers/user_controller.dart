import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/models/qualification.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/models/specification.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/category_repository.dart' as catRepo;

class UserController extends ControllerMVC {

  User user = User(); 
  bool isLoading = false;
  bool displayPassword = false;
  List<Category> categories = <Category>[];
  Category category;

  User currentUser;
  GoogleMapController controller;
  String mapStyle;
  List<Marker> allMarkers = [];

  List<Qualification> qualifications = <Qualification>[];
  Qualification qualification = Qualification();
  List<String> years = <String>[];
  TextEditingController yearController;

  List<Specification> specifications = <Specification>[];
  Specification specification = Specification();


  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> loginFormKey;
  GlobalKey<FormState> qualificationFormKey;
  GlobalKey<FormState> specificationFormKey;

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    qualificationFormKey = new GlobalKey<FormState>();
    specificationFormKey = new GlobalKey<FormState>();
    yearController = TextEditingController();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getCurrentUser() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        this.currentUser = value;
      });
    });
  }

  void getCategories() async {
      setState((){
        isLoading = true;
      });
    if(await isInternetConnection()) {
      setState((){
        categories.clear();
      });
    Stream<Category> stream = await catRepo.getCategories();
    stream.listen((_category) {
      if(_category != null) {
      setState((){
        this.categories.add(_category);
      });
      }
    }, 
    onError: (e){
      print(e);
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error getting Categories list")));
    }, 
    onDone: (){
      setState((){
        isLoading = false;
      });
      });
    } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }

  void login() async {
    if(loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      setState((){
        isLoading = true;
      });
      if(await isInternetConnection()){
        userRepo.login(this.user).then((value) {
          setState((){
            isLoading = false;
          });
          if(value!=null) {
            //Login Success
            if(value.genre == 2 || value.genre == "2"){

              if(value.verified) {
                if (value.qualifications != null &&
                    value.qualifications.length < 0) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      "/Qualification",
                          (Route<dynamic> route) => false,
                      arguments: RouteArgument(currentUser: value));
                }
                else if ((value.lat != null && value.long != null) &&
                    (((value.lat != 0 || value.lat != "0") &&
                        (value.long != 0 || value.long != "0"))
                        || (value.lat.length > 0 && value.long.length > 0))) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      "/Pages", (Route<dynamic> route) => false, arguments: 2);
                }
                else {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      "/UpdateCurrentLocation",
                          (Route<dynamic> route) => false,
                      arguments: RouteArgument(currentUser: value));
                }
              } else {
                //Go for Verification
                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/VerifyEmail",
                        (Route<dynamic> route) => false,
                    arguments: RouteArgument(currentUser: value));
              }
            } else {
              userRepo.logout();
              scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("You're not registered as a nurse")));
            }
          } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong")));
          }
        }, 
        onError: (e){
          print(e);
          setState((){
            isLoading = false;
          });
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error Signing up now")));
        }
        );
      } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
      }
    }
  }

  void register() async {
    if(loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      setState((){
        isLoading = true;
      });
      if(await isInternetConnection()){
        user.catId = this.category.id;
        userRepo.register(this.user).then((value) {
          setState((){
            isLoading = false;
          });
          if(value!=null) {
            if(value.verified) {
              //Register Success
              Navigator.of(context).pushNamedAndRemoveUntil("/Qualification",
                      (Route<dynamic> route) => false,
                  arguments: RouteArgument(currentUser: value));
            } else {
              //Go for Verification
              Navigator.of(context).pushNamedAndRemoveUntil(
                  "/VerifyEmail",
                      (Route<dynamic> route) => false,
                  arguments: RouteArgument(currentUser: value));
            }
          } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong")));
          }
        }, 
        onError: (e){
          print(e);
          setState((){
            isLoading = false;
          });
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error Signing up now")));
        }
        );
      } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
      }
    }
  }

  void updateQualificationSpecs(bool toExp) async {
    setState((){isLoading = true;});
    if(await isInternetConnection()) {
      userRepo.updateUserQualificationSpecs(this.qualifications, this.specifications).then((value) async {
        setState((){isLoading = false;});
        if(value !=null && value.apiToken != null) {
          if(toExp) {
            Navigator.of(context).pushNamedAndRemoveUntil("/Experience", 
              (Route<dynamic> route) => false, 
                arguments: RouteArgument(currentUser: value));
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil("/UpdateCurrentLocation", 
                (Route<dynamic> route) => false, 
                  arguments: RouteArgument(currentUser: value));
          }
        } else {
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong"),));
        }
      }, onError: (e){
        setState((){isLoading = false;});
        print(e);
      });
    } else {
      setState((){isLoading = true;});
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection"),));
    }
  }

  addYears(){
    for(int i=0; i<50; i++) {
      DateTime date = DateTime.now();
      years.add((date.year-i).toString());
    }
  }
  selectYear(String year) {
    setState((){
      yearController.text = year;
    });
  }
  addUserQualification(){
    if(qualificationFormKey.currentState.validate()) {
      qualificationFormKey.currentState.save();
      setState((){
        qualifications.add(qualification);
        qualification = Qualification();
        qualificationFormKey.currentState.reset();
        yearController.text = "";
      });
    }
  }
  removeQualification(int index) {
    setState(() {
      qualifications.removeAt(index);
    });
  }

  //Experience or Specification
  addUserExperience(){
    if(specificationFormKey.currentState.validate()) {
      specificationFormKey.currentState.save();
      setState((){
        specifications.add(specification);
        specification = Specification();
        specificationFormKey.currentState.reset();
      });
    }
  }
  removeUserExperience(int index){
    setState(() {
      specifications.removeAt(index);
    });
  }


  moveNextFromFirstStep(){
    if(category!=null) {
      Navigator.of(context).pushNamed("/Signup", arguments: RouteArgument(category: category));
    }
  }

  initLocationController(){
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }
  moveCamera() {
    setState((){
      allMarkers.clear();
      allMarkers.add(Marker(
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRose),
        markerId: MarkerId('${currentUser.id}'),
        draggable: false,
        infoWindow: InfoWindow(title: currentUser.name, snippet: "You are here"),
        position: LatLng(double.parse(currentUser.lat.toString()) ?? 40.7237765, double.parse(currentUser.long.toString()) ?? -74.017617),));
    });
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(double.parse(currentUser.lat.toString()) ?? 40.7237765,
            double.parse(currentUser.long.toString()) ?? -74.017617),
        zoom: 13.0,
        bearing: 45.0,
        tilt: 45.0)));
  }

}