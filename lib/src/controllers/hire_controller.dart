import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/my_firestore.dart';
import 'package:nurses_app/config/strings.dart';
import 'package:nurses_app/src/models/hire_request.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/hire_repository.dart' as repo;
import 'package:nurses_app/src/repositories/notifications_repository.dart'
    as notiRepo;
import 'package:nurses_app/src/repositories/settings_repository.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class HireController extends ControllerMVC {
  User nurse;
  User currentUser;
  Job job;
  String hireDetailsMessage = "";

  bool isLoading = false;

  TextEditingController descriptionController;
  GlobalKey<FormState> hireFormKey;

  List<HireRequest> requests = <HireRequest>[];

  HireRequest request;
  User sender;

  bool showWithdraw = false;
  bool showAccept = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  HireController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    hireFormKey = new GlobalKey<FormState>();
    descriptionController = TextEditingController();
  }

  void getUserDetails() async {
    userRepo.getUserProfile(this.request.sender.id.toString()).then((_user) {
      if (_user != null) {
        setState(() {
          this.sender = _user;
        });
      }
    }, onError: (e) {
      print(e);
    });
  }

  getMyHireRequests() async {
    MyFirestore.getHireRequests(this.currentUser.id.toString()).listen(
        (_snaps) {
          requests.clear();
          if (_snaps.docs.length > 0) {
            _snaps.docs.forEach((_doc) {

              /*
               Map will be like
               id = requestId
               description = "Job description here"
               job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
               sender = { id: 1, name: Ehtsham, image: http://www.imagelink.com, type: user/hospital, location: GeoPoint() }
               nurse = { id: 1, name: Nurse, image: http://www.imagelink.com, category: categoryName }
               users = [ 1 (senderId) , 2 (nurseId)  ]
               status: "pending"/"accepted"/"rejected"
               scheduled: true/false
               schedule: DateTime in MilliSeconds
               pay: 0.00
              */

              Map<String, dynamic> data = _doc.data();

              HireRequest _req = HireRequest(
                id: data['id'],
                description: data['description'],
                status: data['status'],
                isScheduled: data['scheduled'],
                pay: data['pay']
              );

              if(_req.isScheduled) {
                var date = DateTime.fromMillisecondsSinceEpoch(data['schedule']);
                _req.schedule = DateTime.fromMillisecondsSinceEpoch(data['schedule']);
              }

              if (data['job'] != null) {
                Job _job = Job(
                    id: data['job']['id'],
                    title: data['job']['title'],
                    hours: data['job']['hours'],
                    description: data['job']['description'],);
                _req.job = _job;
              }

              User _nurse = User(
                id: data['nurse']['id'],
                name: data['nurse']['name'],
                image: data['nurse']['image'],
              );

              _req.nurse = _nurse;

              User _sender = User(
                  id: data['sender']['id'],
                  name: data['sender']['name'],
                  image: data['sender']['image'],
                  type: data['sender']['type']);
              var _location = data['sender']['location'] as GeoPoint;
              _sender.lat = _location.latitude;
              _sender.long = _location.longitude;

              _req.sender = _sender;
              _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

              setState(() {
                this.requests.add(_req);
              });
            });
        }
    }, onError: (e) {
      print(e);
    }, onDone: () {});
  }

  acceptRequest() async {

    if(this.currentUser.payMethod == null || this.currentUser.payMethod.cardNumber == null) {
      showLongToast("You must add/update your payment method to start accepting requests");
    }

    else {
      setState(() {
        isLoading = true;
        showAccept = false;
      });
      MyFirestore.getMyTrackingRooms(this.currentUser.id.toString())
          .then((_snaps) {
        if (_snaps.docs.length == 0) {
          repo.acceptHireRequest(this.request.id.toString()).then((value) {
            setState(() {
              isLoading = false;
            });
            if (value.length > 0) {

              //Create Tracking ID
              /*
              Map will be like
              id = requestId
              users = [ 1 (senderId), 2 (nurseId)]
              nursepro = { id: 1, name: nurseName, img: http://www.imagelink.com, categoryName" SomeCategory}
              senderpro = { id: 1, name: userOrHospitalName, img: http://www.imagelink.com, type: user/Hospital}
              >>>For GeoPoints (type will be GeoPoint)
              nurse = [ lat, lng]
              sender = [ lat, lng ]

              description = "Job description here"
               job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
               pay: hirePay
             */

              Map<String, dynamic> _trackMap = Map<String, dynamic>();
              _trackMap['id'] = this.request.id.toString();
              _trackMap['hireId'] = value;
              print(value);

              _trackMap['users'] = [
                this.request.nurse.id.toString(),
                this.request.sender.id.toString()
              ];

              Map<String, String> _nurse = Map<String, String>();
              _nurse['id'] = this.currentUser.id.toString();
              _nurse['name'] = this.currentUser.name.toString();
              _nurse['img'] = this.currentUser.image;
              _nurse['categoryName'] = this.currentUser.category != null
                  ? this.currentUser.category.name
                  : "";

              Map<String, String> _sender = Map<String, String>();
              _sender['id'] = this.request.sender.id.toString();
              _sender['name'] = this.request.sender.name.toString();
              _sender['img'] = this.request.sender.image;
              _sender['type'] = this.request.sender.type.toString();

              if (this.request.job != null) {
                Map<String, dynamic> jobMap = Map<String, dynamic>();
                jobMap['id'] = this.request.job.id;
                jobMap['title'] = this.request.job.title;
                jobMap['description'] = this.request.job.description;
                jobMap['hours'] = this.request.job.hours;

                _trackMap['job'] = jobMap;
              }

              _trackMap['nursepro'] = _nurse;
              _trackMap['senderpro'] = _sender;

              _trackMap['description'] = this.request.description;
              _trackMap['pay'] = this.request.pay;

              _trackMap['sender'] = GeoPoint(this.request.sender.lat, this.request.sender.long);
              _trackMap['nurse'] = GeoPoint(
                  double.parse(this.currentUser.lat.toString()),
                  double.parse(this.currentUser.long.toString()));

              MyFirestore.createTrackingRoom(_trackMap, this.request.id.toString());
              MyFirestore.deleteHireRequest(this.request.id.toString());

              String title = "Hire Request Accepted";
              String body = "${this.currentUser
                  .name} accepted your hire request and on her way.";

              notiRepo.sendNotification(body, title, this.sender.firebaseToken);

              Navigator.of(context).pushReplacementNamed(
                  "/trackLocation",
                  arguments:
                  RouteArgument(trackingReqId: this.request.id.toString()));
            }
          }, onError: (e) {
            print(e);
            scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("Verify your internet connection")));
          });
        } else {
          showToast("You already have an unfinished tracking");
        }
      }, onError: (e) {
        setState(() {
          isLoading = false;
        });
        print(e);
      });
    }
  }

  withdrawRequest() async {
    setState(() {
      isLoading = true;
      showWithdraw = false;
    });
    repo.rejectHireRequest(this.request.id).then((value) {
      if(value) {
        MyFirestore.deleteHireRequest(this.request.id);
        String title = "Hire Request Rejected";
        String body = "${this.currentUser.name} rejected your hire request";
        notiRepo.sendNotification(body, title, this.sender.firebaseToken);
        Navigator.of(context).pop();
      }
    },
        onError: (e){
          print(e);
          showToast("Error rejecting Request");
        });

  }

  //INSTANT REQUESTS
  List<HireRequest> instantRequests = <HireRequest>[];
  getMyInstantHireRequests() async {
    print('Getting');
    MyFirestore.getInstantHireRequestsByCategory(this.currentUser?.category?.id.toString()).listen((_snaps){
      instantRequests.clear();
      _snaps.docs.forEach((_doc) {
        Map<String, dynamic> data = _doc.data();

        HireRequest _req = HireRequest(
          id: data['id'],
          category: data['category'],
          categoryName: data['categoryName'],
          status: data['status'],
          isInstant: true,
        );

        User _sender = User(id: data['sender']['id'], name: data['sender']['name'],
            image: data['sender']['image'], type: data['sender']['type']);

        GeoPoint _senderGeoPoint = (data['sender']['location'] as GeoPoint);
        if(_senderGeoPoint != null) {
          LatLng _senderLoc = LatLng(
              _senderGeoPoint?.latitude, _senderGeoPoint?.longitude);

          _sender.lat = _senderLoc.latitude;
          _sender.long = _senderLoc.longitude;
        }

        _req.sender = _sender;
        _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

        if(this.instantRequests.where((element) => element.id == _req.id).isEmpty) {
          setState(() {
            this.instantRequests.add(_req);
          });
        }

      });
      setState((){});
    },
        onError: (e){
          print(e);
        },
        onDone: (){}
    );
  }

  acceptInstantRequest(HireRequest _req) async {

    if(!this.isLoading) {
      if (this.currentUser.payMethod == null ||
          this.currentUser.payMethod.cardNumber == null) {
        showLongToast(
            "You must add/update your payment method to start accepting requests");
      }
      else {
        setState(() {
          isLoading = true;
          showAccept = false;
        });
        MyFirestore.getMyTrackingRooms(this.currentUser.id.toString())
            .then((_snaps) async {
          if (_snaps.docs.length == 0) {
            setState(() {
              isLoading = true;
            });
            final _senderRes = await userRepo.getUserProfile(
                _req.sender.id.toString());
            //Create Tracking ID
            /*
              Map will be like
              id = requestId
              users = [ 1 (senderId), 2 (currentUserId)]
              nursepro = { id: 1, name: nurseName, img: http://www.imagelink.com, categoryName" SomeCategory}
              senderpro = { id: 1, name: userOrHospitalName, img: http://www.imagelink.com, type: user/Hospital}
              >>>For GeoPoints (type will be GeoPoint)
              nurse = [ lat, lng]
              sender = [ lat, lng ]

              description = "Instant Request"
               pay: 0
             */

            Map<String, dynamic> _trackMap = Map<String, dynamic>();
            _trackMap['id'] = _req.id.toString();
            _trackMap['hireId'] = '123123';
            //TODO: For Instant Requests
            _trackMap['isInstant'] = true;
            // print(value);

            _trackMap['users'] = [
              this.currentUser.id.toString(),
              _req.sender.id.toString()
            ];

            Map<String, String> _nurse = Map<String, String>();
            _nurse['id'] = this.currentUser.id.toString();
            _nurse['name'] = this.currentUser.name.toString();
            _nurse['img'] = this.currentUser.image;
            _nurse['categoryName'] = this.currentUser.category != null
                ? this.currentUser.category.name
                : "";

            Map<String, String> _sender = Map<String, String>();
            _sender['id'] = _req.sender.id.toString();
            _sender['name'] = _req.sender.name.toString();
            _sender['img'] = _req.sender.image;
            _sender['type'] = _req.sender.type.toString();

            _trackMap['nursepro'] = _nurse;
            _trackMap['senderpro'] = _sender;

            _trackMap['description'] = 'Hired for Instant Request';
            _trackMap['pay'] = '0';

            _trackMap['sender'] = GeoPoint(_req.sender.lat, _req.sender.long);
            _trackMap['nurse'] = GeoPoint(
                double.parse(this.currentUser.lat.toString()),
                double.parse(this.currentUser.long.toString()));

            setState(() {
              isLoading = false;
            });
            MyFirestore.createTrackingRoom(_trackMap, _req.id.toString());
            MyFirestore.deleteInstantHireRequest(_req.id.toString());

            String title = "Hire Request Accepted";
            String body = "${this.currentUser
                .name} accepted your hire request and on her way.";

            notiRepo.sendNotification(body, title, _senderRes.firebaseToken);

            Navigator.of(context).pushReplacementNamed(
                "/trackLocation",
                arguments:
                RouteArgument(trackingReqId: _req.id.toString()));
          }
          else {
            showToast("You already have an unfinished tracking");
          }
        }, onError: (e) {
          setState(() {
            isLoading = false;
          });
          print(e);
        });
      }
    }
  }

}
