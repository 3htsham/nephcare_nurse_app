import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:nurses_app/src/repositories/job_repository.dart' as jobRepo;

class JobController extends ControllerMVC {

  User currentUser;
  Job job;
  bool isLoading = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  JobController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getCurrentUser() async {
    userRepo.getCurrentUser().then((_user) {
      setState(() {
        currentUser = _user;
      });
    });
  }

  applyOnJob() async {
    setState((){ isLoading = true;});
    jobRepo.applyOnJob(job).then((value) {
      setState((){ isLoading = false;});
      if(value) {
        this.job.applied = true;
        showToast("Applied Successfully");
      }
    },
        onError: (e){
      print(e);
          setState((){ isLoading = false;});
    });
  }

}