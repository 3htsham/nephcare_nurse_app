class Helper {

  static getCategoriesData(Map<String, dynamic> data) {
    return data['categories'] ?? [];
  }

  static getJobsData(Map<String, dynamic> data) {
    return data['jobs'] ?? [];
  }

  static getMessageData(Map<String, dynamic> data) {
    return data['message'] ?? [];
  }
  static getUserData(Map<String, dynamic> data) {
    return data['user'] ?? [];
  }
  static getHospitalsData(Map<String, dynamic> data) {
    return data['nearby_hospitals'] ?? [];
  }

}