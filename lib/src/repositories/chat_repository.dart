import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:global_configuration/global_configuration.dart';
import 'package:nurses_app/src/helpers/helper.dart';
import 'package:nurses_app/src/models/chat.dart';
import 'package:nurses_app/src/models/message.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;


Future<Message> sendMessage(Message msg) async {
  Message message; //If true (error deleting request, else deleted)
  User _user = await userRepo.getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}message/send?$_apiToken';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(msg.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    var data = Helper.getMessageData(json.decode(body));
    message = Message.fromJson(data);
  }
  else {
    var error = response.body;
    print(error);
  }
  return message;
}

Future<bool> deleteAChat(Chat chat) async {
  bool sentError = true; //If true (error deleting request, else deleted)
  User _user = await userRepo.getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/chat/delete?$_apiToken';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(chat.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['error'] != null) {
      sentError = json.decode(body)['error'];
    }
  }
  return sentError;
}