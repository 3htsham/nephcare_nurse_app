import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:nurses_app/src/helpers/helper.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;


Future<Stream<Job>> getJobs() async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}fetch/jobs$_apiToken';
  
  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getJobsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Job.fromJson(data);}
      );
}

Future<bool> applyOnJob(Job _job) async {
  bool isApplied = false;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _userId = '&user_id=${_currentUser.id}';
  var _jobId = '&job_id=${_job.id}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}apply/request$_apiToken$_userId$_jobId';
  var client = http.Client();final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = json.decode(response.body);
    if(body['error'] == false || body['error'] == "false") {
      isApplied = true;
    }
  }
  return isApplied;
}

Future<Stream<Job>> getUserJobs(var userId) async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String _userId = '&user_id=$userId';
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/jobs$_apiToken$_userId';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getJobsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Job.fromJson(data);}
  );
}