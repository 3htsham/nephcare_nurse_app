import 'dart:convert';
import 'dart:io';
import 'package:nurses_app/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:global_configuration/global_configuration.dart';

Future<bool> sendHireRequest(String nurseId, var jobId, String description) async {
  bool isSent = false;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}hire/request';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['nurse_id'] = nurseId;
  if(jobId != null) {
    bodyMap['job_id'] = jobId;
  }
  bodyMap['description'] = description;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      isSent = true;
    }
  }
  return isSent;

}


Future<String> acceptHireRequest(String requestId) async {
  String hireId = "";
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}hire/request/accept';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['hire_request_id'] = requestId;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      hireId = json.decode(response.body)['hired']["id"].toString();
    }
  }
  return hireId;

}

Future<bool> rejectHireRequest(var hireRequestId) async {
  bool isWithdraw = false;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}reject/request';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['hire_request_id'] = hireRequestId;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      var body = response.body;
      isWithdraw = true;
    }
  }
  return isWithdraw;

}