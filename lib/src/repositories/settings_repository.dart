import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:shared_preferences/shared_preferences.dart';

ValueNotifier<Locale> locale = new ValueNotifier(Locale('en', ''));


toggleNotificationsStatus(bool isOn) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setBool('isNoti', isOn);
}


Future<bool> getNotificationsStatus() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if(prefs.containsKey("isNoti")) {
    return prefs.getBool("isNoti");
  } else {
    await prefs.setBool('isNoti', true);
    return true;
  }
}


Future<bool> isInternetConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    else
    {
      return false;
    }
  } on SocketException catch (_) {
    return false;
  }
}

showToast(String text) {
  return Fluttertoast.showToast(
    msg: text,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: config.Colors().scaffoldColor(1),
    textColor: config.Colors().textColor(1),
    fontSize: 13.0,
  );
}

showLongToast(String text) {
  return Fluttertoast.showToast(
    msg: text,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    backgroundColor: config.Colors().scaffoldColor(1),
    textColor: config.Colors().textColor(1),
    fontSize: 13.0,
  );
}