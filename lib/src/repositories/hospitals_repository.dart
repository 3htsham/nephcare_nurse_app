import 'dart:convert';
import 'package:nurses_app/src/helpers/helper.dart';
import 'package:nurses_app/src/models/user.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

Future<Stream<User>> getNearbyHospitals() async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}nearby/hospital$_apiToken';
  final client = new http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getHospitalsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return User.fromJson(data);}
  );
}