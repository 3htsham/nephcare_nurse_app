import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart';
import 'package:nurses_app/src/neumorphic/nue_check_box.dart';

class ChooseCategoryWidget extends StatefulWidget {
  @override
  _ChooseCategoryWidgetState createState() => _ChooseCategoryWidgetState();
}

class _ChooseCategoryWidgetState extends StateMVC<ChooseCategoryWidget> {

  UserController _con;

  _ChooseCategoryWidgetState(): super(UserController()){
    _con = controller;
  }

  @override
  void initState() {
    _con.getCategories();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: Container(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 30,),
                  Hero(
                    tag: "Logo",
                    child: Align(
                        alignment: Alignment.center,
                        child: Image.asset("assets/img/logo.png", width: 130,)
                    ),
                  ),
                  SizedBox(height: 10,),

                  Hero(tag: "welcome" ,child: Text("LET US KNOW!",
                    textAlign: TextAlign.center,
                    style: textTheme.headline5,),
                  ),
                  Text("HOW DO YOU WANT TO CONTINUE!",
                    textAlign: TextAlign.center,
                    style: textTheme.subtitle1.merge(TextStyle(fontSize: 12)),),
                  SizedBox(height: 25,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    child: Column(
                      children: [

                        _con.categories.isNotEmpty 
                        ? NeuTextFieldContainer(
                          borderRadius: BorderRadius.circular(15),
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          textField: Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              padding: EdgeInsets.symmetric(vertical: 10),
                              itemCount: _con.categories.length,
                                itemBuilder: (context, index) {
                                Category category = _con.categories[index];
                                  return InkWell(
                                    onTap: (){
                                      if(!_con.isLoading) {
                                      setState((){
                                        _con.category = category;
                                      });
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(vertical: 5),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text(category.name, style: textTheme.bodyText1,),
                                          Spacer(),
                                          NeuBtnCheckBox(
                                            onPressed: (){
                                              if(!_con.isLoading) {
                                                setState((){
                                                  _con.category = category;
                                                });
                                              }
                                            },
                                            state: _con.category != null && _con.category.id==category.id,
                                            borderRadius: 100,
                                            width: 30,
                                            height: 30,
                                            child: Icon(CupertinoIcons.check_mark, color: _con.category != null && _con.category.id==category.id ? theme.accentColor : Colors.white,),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }
                            ),
                          ),
                        )
                        : CircularLoadingWidget(height: 100),
                        
                        SizedBox(height: 15,),
                      ],
                    ),
                  ),

                  SizedBox(height: 15,),

                  !_con.isLoading ? Hero(
                    tag: "LoginSignupButton",
                    child: NeuButton(
                      width: 150,
                      height: 50,
                      text: "NEXT".toUpperCase(),
                      textStyle: textTheme.subtitle2,
                      borderRadius: 100,
                      onPressed: (){
                        _con.moveNextFromFirstStep();
                      },
                    ),
                  ) : NeuContainer(
                  width: 50,
                    height: 50,
                    borderRadius: 100,
                    child: CircularProgressIndicator()),

                  SizedBox(height: 20,),
                  Hero(tag: "OR",child: Text("OR", style: textTheme.caption,)),
                  SizedBox(height: 20,),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pushReplacementNamed("/Login");
                    },
                    splashColor: theme.primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("LOGIN", style: textTheme.bodyText1,),
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        )
    );
  }

}
