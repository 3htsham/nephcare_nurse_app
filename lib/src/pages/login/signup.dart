import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart';

class SignUp extends StatefulWidget {
  RouteArgument argument;

  SignUp({this.argument});

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends StateMVC<SignUp> {

  UserController _con;

  _SignUpState() : super(UserController()) {
    _con = controller;
  }
  @override
  void initState() {
    _con.category = widget.argument.category;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      key: _con.scaffoldKey,
        body: Container(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 30,),
                  Hero(
                    tag: "Logo",
                    child: Align(
                        alignment: Alignment.center,
                        child: Image.asset("assets/img/logo.png", width: 100,)
                    ),
                  ),
                  SizedBox(height: 15,),

                  Hero(tag: "welcome" ,child: Text("PEOPLE ARE\nLOOKING FOR YOU!",
                    textAlign: TextAlign.center,
                    style: textTheme.headline5,),
                  ),
                  SizedBox(height: 25,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    child: Form(
                      key: _con.loginFormKey,
                      child: Column(
                        children: [
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Name must not be empty":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Name",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.person, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.user.name = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              textField: TextFormField(
                              enabled: !_con.isLoading,
                                validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                  prefixIcon: Icon(CupertinoIcons.mail, color: config.Colors().textColor(1),),
                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                ),
                                onSaved: (val){
                                _con.user.email = val;
                                },
                              ),
                            ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Enter Valid License No" : null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "License No",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.news, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.user.licenseNo = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Password mus not be empty":null,
                              obscureText: true,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Password",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.padlock, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                                  suffixIcon: IconButton(
                                    padding: EdgeInsets.all(0),
                                    onPressed: (){
                                      setState((){
                                        _con.displayPassword = !_con.displayPassword;
                                      });
                                    },
                                    icon: Icon(_con.displayPassword
                                        ? Icons.visibility_off_outlined
                                        : Icons.visibility_outlined,
                                      color: theme.focusColor,),
                                  )
                              ),
                              onSaved: (val){
                                _con.user.password = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 15,),
                  _con.isLoading 
                  ? NeuContainer(
                  width: 50,
                    height: 50,
                    borderRadius: 100,
                    child: CircularProgressIndicator())
                  : Hero(
                    tag: "LoginSignupButton",
                    child: NeuButton(
                      width: 150,
                      height: 50,
                      text: "SIGNUP".toUpperCase(),
                      textStyle: textTheme.subtitle2,
                      borderRadius: 100,
                      onPressed: (){
                        _con.register();
                      },
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        )
    );
  }
}
