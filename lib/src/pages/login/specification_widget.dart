import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/experience/experience_list_container.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class SpecificationWidget extends StatefulWidget {

  RouteArgument argument;
  
  SpecificationWidget({this.argument});

  @override
  _SpecificationidgetState createState() => _SpecificationidgetState();
}

class _SpecificationidgetState extends StateMVC<SpecificationWidget> {

  UserController _con;


  _SpecificationidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    
    return Scaffold(
        key: _con.scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: Container(
          child: Center(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  SizedBox(height: 20,),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Hero(
                          tag: "Logo",
                          child: Align(
                              alignment: Alignment.center,
                              child: Image.asset("assets/img/experience.png", width: 100,)
                          ),
                        ),
                        SizedBox(width: 15,),
                        Hero(tag: "welcome" ,child: Text("EXPERIENCE\nOR SPECIALIZATION!",
                          textAlign: TextAlign.left,
                          style: textTheme.headline5,),
                        ),],
                    ),
                  ),
                  SizedBox(height: 25,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    child: Form(
                      key: _con.specificationFormKey,
                      child: Column(
                        children: [
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Title can't be empty":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Title",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(Icons.stars, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.specification.specName = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Specify Experience (can be 0)":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Years of Experience",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(Icons.account_balance, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.specification.exp = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          NeuButton(
                            // width: 50,
                            height: 50,
                            text: "ADD".toUpperCase(),
                            textStyle: textTheme.subtitle2,
                            borderRadius: 100,
                            onPressed: (){
                              if(!_con.isLoading) {
                                _con.addUserExperience();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),


                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                    child: _con.specifications.isEmpty 
                    ? Container(height: 160,) 
                    : ExperienceListContainer(con: _con),
                  ),

                  SizedBox(height: 15,),
                  _con.isLoading 
                  ? NeuContainer(
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    child: CircularProgressIndicator())
                    : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FlatButton(
                        onPressed: (){
                          if(!_con.isLoading) {
                            Navigator.of(context).pushNamedAndRemoveUntil("/UpdateCurrentLocation", 
                              (Route<dynamic> route) => false, 
                              arguments: RouteArgument(currentUser: _con.currentUser));
                          }
                        },
                        child: Center(
                          child: Text("SKIP", style: textTheme.subtitle2,),
                        ),
                      ),
                      Hero(
                        tag: "LoginSignupButton",
                        child: NeuButton(
                          width: 150,
                          height: 50,
                          text: "CONTINUE".toUpperCase(),
                          textStyle: textTheme.subtitle2,
                          borderRadius: 100,
                          onPressed: (){
                            if(!_con.isLoading){
                              _con.updateQualificationSpecs(false);
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                ],
              ),
            ),
          ),
        ),
    );
  }
}