import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends StateMVC<Login> {
  UserController _con;

  _LoginState() : super(UserController()) {
    _con = controller;
  }



  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
        key: _con.scaffoldKey,
      resizeToAvoidBottomInset: false,
        body: Container(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 30,),
                  Hero(
                    tag: "Logo",
                    child: Align(
                        alignment: Alignment.center,
                        child: Image.asset("assets/img/logo.png", width: 100,)
                    ),
                  ),
                  SizedBox(height: 15,),
                  Hero(tag: "welcome" ,child: Text("WELCOME BACK", style: textTheme.headline5,)),
                  SizedBox(height: 25,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    child: Form(
                      key: _con.loginFormKey,
                      child: Column(
                        children: [
                          Hero(
                            tag: "EmailField",
                            child: NeuTextFieldContainer(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              textField: TextFormField(
                                enabled: !_con.isLoading,
                                keyboardType: TextInputType.emailAddress,
                                validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                  prefixIcon: Icon(CupertinoIcons.mail, color: config.Colors().textColor(1),),
                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                ),
                                onSaved: (val){
                                  _con.user.email = val;
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 15,),
                          Hero(
                            tag: "PasswordField",
                            child: NeuTextFieldContainer(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              textField: TextFormField(
                                enabled: !_con.isLoading,
                                validator: (val) => val.isEmpty ? "Password mus not be empty":null,
                                obscureText: true,
                                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                  prefixIcon: Icon(CupertinoIcons.padlock, color: config.Colors().textColor(1),),
                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                                    suffixIcon: IconButton(
                                      padding: EdgeInsets.all(0),
                                      onPressed: (){
                                        setState((){
                                          _con.displayPassword = !_con.displayPassword;
                                        });
                                      },
                                      icon: Icon(_con.displayPassword
                                          ? Icons.visibility_off_outlined
                                          : Icons.visibility_outlined,
                                        color: theme.focusColor,),
                                    )
                                ),
                                onSaved: (val){
                                  _con.user.password = val;
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),
                          Align(
                            alignment: Alignment.centerRight,
                            child: InkWell(
                              onTap: (){
                              if(!_con.isLoading) {}
                              },
                              splashColor: theme.primaryColor,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5),
                                child: Text("Forget Password?", style: textTheme.bodyText1,),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 15,),
                  _con.isLoading 
                  ? NeuContainer(
                  width: 50,
                    height: 50,
                    borderRadius: 100,
                    child: CircularProgressIndicator())
                    : Hero(
                    tag: "LoginSignupButton",
                    child: NeuButton(
                      width: 150,
                      height: 50,
                      text: "Login".toUpperCase(),
                      textStyle: textTheme.subtitle2,
                      borderRadius: 100,
                      onPressed: (){
                        _con.login();
                      },
                    ),
                  ),
                  SizedBox(height: 30,),
                  Hero(tag: "OR",child: Text("OR", style: textTheme.caption,)),
                  SizedBox(height: 30,),
                  InkWell(
                    onTap: (){
                      if(!_con.isLoading) {
                        Navigator.of(context).pushNamed("/ChooseCategory");
                      }
                    },
                    splashColor: theme.primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("SIGNUP", style: textTheme.bodyText1,),
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        )
    );
  }
}
