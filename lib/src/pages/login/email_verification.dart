import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/verification_controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class EmailVerificationWidget extends StatefulWidget {
  RouteArgument argument;
  EmailVerificationWidget({this.argument});

  @override
  _EmailVerificationWidgetState createState() => _EmailVerificationWidgetState();
}

class _EmailVerificationWidgetState extends StateMVC<EmailVerificationWidget> {

  VerificationController _con;

  _EmailVerificationWidgetState() : super(VerificationController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      body: Container(
        width: size.width,
        height: size.height,
        color: theme.scaffoldBackgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 60,),
              Align(
                  alignment: Alignment.center,
                  child: _con.isVerified
                      ? Image.asset("assets/img/verified.png", width: 100, color: theme.accentColor,)
                      : Image.asset("assets/img/not_verified.png", width: 100, color: theme.focusColor,)
              ),
              SizedBox(height: 15,),

              Text(_con.isVerified  ? "VERIFIED" : "VERIFY YOUR EMAIL",
                textAlign: TextAlign.center,
                style: textTheme.headline5,),
              SizedBox(height: 10,),
              Text("We 've just sent a code to your email.\nKindly enter the code below\nto verify your email",
                textAlign: TextAlign.center,
                style: textTheme.bodyText2,),
              SizedBox(height: 25,),
              NeuTextFieldContainer(
                padding: EdgeInsets.symmetric(horizontal: 5),
                textField: TextFormField(
                  enabled: !_con.isLoading,
                  controller: _con.controller,
                  validator: (val) => val.isEmpty ? "Verification Code must not be empty":null,
                  style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                  keyboardType: TextInputType.text,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Verification Code",
                    hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                    prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                  ),
                ),
              ),
              SizedBox(height: 15,),
              _con.isLoading
                  ? NeuContainer(
                  width: 50,
                  height: 50,
                  borderRadius: 100,
                  child: CircularProgressIndicator())
                  :
              Hero(
                tag: "LoginSignupButton",
                child: NeuButton(
                  width: 150,
                  height: 50,
                  text: "VERIFY".toUpperCase(),
                  textStyle: textTheme.subtitle2,
                  borderRadius: 100,
                  onPressed: (){
                    _con.verify();
                  },
                ),
              ),
              SizedBox(height: 20,),

            ],
          ),
        ),
      ),
    );
  }
}
