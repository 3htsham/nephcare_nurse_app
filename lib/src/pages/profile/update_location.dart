import 'package:nurses_app/src/controllers/location_controller.dart';
import 'package:nurses_app/src/elements/location/location_search_widget.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:location/location.dart' as loc;
import 'package:nurses_app/config/app_config.dart' as config;

class UpdateCurrentLocation extends StatefulWidget {

  RouteArgument argument;

  UpdateCurrentLocation({this.argument});

  @override
  _ChooseCurrentLocationState createState() => _ChooseCurrentLocationState();
}

class _ChooseCurrentLocationState extends StateMVC<UpdateCurrentLocation> {

  String _titleText = "Your Current location";
  var location = loc.Location();
  loc.PermissionStatus _status;
  LocationController _con;

  _ChooseCurrentLocationState() : super(LocationController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.initiateLocationController();
    location.hasPermission()
        .then(_updateStatus);
  }

  @override
  Widget build(BuildContext context) {

    _con.createMarker(context);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(target: LatLng(40.7237765, -74.017617), zoom: 13.0),
              // markers: markers,
              onTap: (pos) {
                _con.getAddressAndLocation(pos);
              },
              markers: Set.from(_con.allMarkers),
              onMapCreated: (GoogleMapController controller) {
                _con.isMapCreated = true;
                _con.controller = controller;
                _con.controller.setMapStyle(_con.mapStyle);
              },
            ),
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Container(
                  height: 75.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: config.Colors().scaffoldColor(1),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 30.0),
                    child: Center(
                      child: Text(
                          _titleText,
                          style: textTheme.subtitle2.merge(TextStyle(color: theme.accentColor))
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 40.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: <Color>[
                      config.Colors().scaffoldColor(0.1),
                      config.Colors().scaffoldColor(0.6),
                      config.Colors().scaffoldColor(1),
                    ],
                  ),
                ),
              ),
              LocationSearchWidget(
                  displayPrediction:(p){ _con.displayPrediction(p);},
                  searchedText: _con.searchedText
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: _con.isLocationPicked
          ? FloatingActionButton(
        onPressed: (){
          ///Update User's Location
          if(!_con.isLoading) {
            _con.updateUserInApp();
          }
        },
        backgroundColor: theme.scaffoldBackgroundColor,
        child: _con.isLoading ? CircularProgressIndicator() : Icon(Icons.arrow_forward, color: theme.accentColor),
      )
          : SizedBox(height: 0, width: 0),
    );
  }


  void _updateStatus(loc.PermissionStatus status) async {
    if(_status != status) {
      setState(() {
        _status: status;
      });
      //Check if user location turned On or not
      //then call this function
      _checkLocationServiceStatus();
    } else {
      _askPermission();
    }
  }

  void _askPermission() {
    if (_status == null) {
      location.requestPermission().then((status) {
        if(status == loc.PermissionStatus.granted) {
          setState(() {
            _status = status;
            _titleText = "Your Current location";
          });
          _checkLocationServiceStatus();
        } else {
          //Start activity to ask to enter it manually
          setState(() {
            _titleText = "Pick your current location";
          });
        }
      });
    }
  }

  void _checkLocationServiceStatus() async {
    if(await location.serviceEnabled())
    {
      _con.getUserLocation();
    }
    else
    {
      if(await location.requestService())
      {
        _con.getUserLocation();
      }
      else
      {
        //Enter your Location Manually
        setState(() {
          _titleText = "Search your location manually";
        });
      }
    }
  }

}