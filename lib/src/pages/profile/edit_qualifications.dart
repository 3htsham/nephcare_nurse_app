import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/QualAndSpecsController.dart';
import 'package:nurses_app/src/elements/profile/my_qualification_list_edit.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';

class EditQualificationWidget extends StatefulWidget {
  RouteArgument argument;
  EditQualificationWidget({this.argument});

  @override
  _EditQualificationWidgetState createState() => _EditQualificationWidgetState();
}

class _EditQualificationWidgetState extends StateMVC<EditQualificationWidget> {
  QualAndSpecsController _con;
  GlobalKey yearKey;
  bool isDropDownOpened = false;
  double height, width, xPos, yPos;
  OverlayEntry floatingDropDown;

  _EditQualificationWidgetState() : super(QualAndSpecsController()) {
    _con = controller;
  }

  @override
  void initState() {
    yearKey = new LabeledGlobalKey("Year");
    _con.currentUser = widget.argument.currentUser;
    if(_con.currentUser.qualifications != null) {
      _con.qualifications = _con.currentUser.qualifications;
    }
    if(_con.currentUser.specifications != null) {
      _con.specifications = _con.currentUser.specifications;
    }
    _con.addYears();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: config.Colors().appBar(context, "Edit Qualifications"),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 10,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Text("YOUR\nQUALIFICATIONS!",
                  textAlign: TextAlign.center,
                  style: textTheme.headline5,),
              ),
              SizedBox(height: 25,),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                alignment: Alignment.center,
                child: Form(
                  key: _con.qualificationFormKey,
                  child: Column(
                    children: [
                      NeuTextFieldContainer(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        textField: TextFormField(
                          enabled: !_con.isLoading,
                          validator: (val) => val.isEmpty ? "Degree name must not be Empty":null,
                          style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Degree",
                            hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                            prefixIcon: Icon(Icons.stars, color: config.Colors().textColor(1),),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                          onSaved: (val){
                            _con.qualification.degree = val;
                          },
                        ),
                      ),
                      SizedBox(height: 15,),
                      NeuTextFieldContainer(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        textField: TextFormField(
                          enabled: !_con.isLoading,
                          validator: (val) => val.isEmpty ? "Specify an Institute ":null,
                          style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Institute",
                            hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                            prefixIcon: Icon(Icons.account_balance, color: config.Colors().textColor(1),),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                          onSaved: (val){
                            _con.qualification.institute = val;
                          },
                        ),
                      ),
                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              key: yearKey,
                              onTap: (){
                                if(!_con.isLoading) {
                                  this._dropDownBuilder();
                                }
                              },
                              child: NeuTextFieldContainer(
                                padding: EdgeInsets.symmetric(horizontal: 50),
                                textField: TextFormField(
                                  onTap: (){},
                                  controller: _con.yearController,
                                  enabled: false,
                                  validator: (val) => val.isEmpty ? "Select year":null,
                                  style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                  decoration: InputDecoration(
                                    errorStyle: textTheme.caption,
                                    border: InputBorder.none,
                                    hintText: "Year",
                                    hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                    prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                  ),
                                  onSaved: (val){
                                    _con.qualification.year = val;
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 10,),
                          NeuButton(
                            // width: 50,
                            height: 50,
                            text: "ADD".toUpperCase(),
                            textStyle: textTheme.subtitle2,
                            borderRadius: 100,
                            onPressed: (){
                              if(!_con.isLoading){
                                _con.addUserQualification();
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                child: _con.qualifications.isEmpty
                    ? Container(height: 160,)
                    : MyQualificationsListEditContainer(con: _con),
              ),
              SizedBox(height: 15,),
              _con.isLoading
                  ? NeuContainer(
                  width: 50,
                  height: 50,
                  borderRadius: 100,
                  child: CircularProgressIndicator())
                  : NeuButton(
                    width: 150,
                    height: 50,
                    text: "UPDATE".toUpperCase(),
                    textStyle: textTheme.subtitle2,
                    borderRadius: 100,
                    onPressed: (){
                      if(!_con.isLoading) {
                        _con.updateQualificationSpecs();
                      }
                    },
                  ),
              SizedBox(height: 10,),

            ],
          ),
        ),
      ),
    );
  }


  _dropDownBuilder(){
    setState((){
      if(isDropDownOpened) {
        floatingDropDown.remove();
      } else {
        findDropDownData();

        floatingDropDown = _createFloatingDropDown();
        Overlay.of(context).insert(floatingDropDown);
      }
      isDropDownOpened = !isDropDownOpened;
    });
  }

  findDropDownData(){
    RenderBox renderBox = yearKey.currentContext.findRenderObject();
    height = renderBox.size.height;
    width = renderBox.size.width;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    xPos = offset.dx;
    yPos = offset.dy;
  }

  OverlayEntry _createFloatingDropDown(){
    return OverlayEntry(builder: (context) {
      return Positioned(
        top: yPos+ height,
        left: xPos,
        width: width,
        height: 4*height+15,
        child: Padding(
          padding: EdgeInsets.only(top: 10),
          child: NeuContainer(
            height: 20,
            width: width-10,
            borderRadius: 10,
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                  children: List.generate(
                      _con.years.length,
                          (index) {
                        return Material(
                          color: Colors.transparent,
                          elevation: 0,
                          child: InkWell(
                            onTap: (){
                              _con.selectYear(_con.years[index]);
                              setState((){
                                floatingDropDown.remove();
                                isDropDownOpened = false;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                              child: Column(
                                children: [
                                  Text(_con.years[index], style: Theme.of(context).textTheme.bodyText1,),
                                  SizedBox(height: 10,),
                                  Container(height: 0.2, color: Theme.of(context).focusColor.withOpacity(0.6),)
                                ],
                              ),
                            ),
                          ),
                        );
                      })
              ),
            ),
          ),
        ),
      );
    });
  }
}
