import 'dart:io';
import 'package:nurses_app/src/controllers/profile_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/models/category.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_button_container.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/neumorphic/nue_check_box.dart';

class EditProfile extends StatefulWidget {
  RouteArgument argument;
  EditProfile({this.argument});

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends StateMVC<EditProfile> {
  ProfileController _con;

  _EditProfileState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(_con.showDialog) {
          setState((){ _con.showDialog = false; });
          return false;
        } else if(_con.isLoading) {
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: config.Colors().appBar(context, "Edit Profile"),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          height: size.height,
          width: size.width,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        NeuTextFieldContainer(
                          padding: EdgeInsets.all(7),
                          textField: InkWell(
                            onTap: (){
                              if(!_con.isLoading) {
                                _con.showPhotoDialog();
                              }
                            },
                            child: NeuContainer(
                              borderRadius: 100,
                              width: 120,
                              height: 120,
                              child: Padding(
                                padding: EdgeInsets.all(3),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: _con.currentUser != null &&
                                          !_con.currentUser.isNewPhoto &&
                                          _con.currentUser.image != null &&
                                          _con.currentUser.image.length > 1
                                      ? FadeInImage.assetNetwork(
                                          placeholder:
                                              "assets/img/placeholders/profile.png",
                                          image: _con.currentUser.image,
                                          width: 120,
                                          height: 120,
                                          fit: BoxFit.cover,
                                        )
                                      : _con.currentUser.isNewPhoto
                                          ? Image.file(
                                              File(
                                                _con.currentUser
                                                    .newPhotoIdentifier,
                                              ),
                                              width: 120,
                                              height: 120,
                                              fit: BoxFit.cover,
                                            )
                                          : Image.asset(
                                              "assets/img/placeholders/profile_placeholder.png",
                                              width: 120,
                                              height: 120,
                                              fit: BoxFit.cover,
                                            ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Form(
                      key: _con.editFormKey,
                      child: Column(
                        children: [

                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                            textField: TextFormField(
                              initialValue: _con.currentUser.name ?? "",
                              enabled: !_con.isLoading,
                              validator: (val) =>
                              (val.isEmpty) ? "Name must not be empty" : null,
                              style: textTheme.bodyText1.merge(
                                  TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Name",
                                hintStyle: textTheme.bodyText1
                                    .merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(
                                  CupertinoIcons.person,
                                  color: config.Colors().textColor(1),
                                ),
                                prefixIconConstraints:
                                BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val) {
                                _con.currentUser.name = val;
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                            textField: TextFormField(
                              initialValue: _con.currentUser.email ?? "",
                              enabled: !_con.isLoading,
                              validator: (val) => (val.isEmpty && !val.contains("@"))
                                  ? "Email must not be empty"
                                  : null,
                              style: textTheme.bodyText1.merge(
                                  TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Email",
                                hintStyle: textTheme.bodyText1
                                    .merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(
                                  CupertinoIcons.mail,
                                  color: config.Colors().textColor(1),
                                ),
                                prefixIconConstraints:
                                BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val) {
                                _con.currentUser.email = val;
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                            textField: TextFormField(
                              initialValue: _con.currentUser.phone ?? "",
                              enabled: !_con.isLoading,
                              validator: (val) =>
                              (val.isEmpty) ? "Phone must not be empty" : null,
                              style: textTheme.bodyText1.merge(
                                  TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Phone",
                                hintStyle: textTheme.bodyText1
                                    .merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(
                                  CupertinoIcons.phone,
                                  color: config.Colors().textColor(1),
                                ),
                                prefixIconConstraints:
                                BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val) {
                                _con.currentUser.phone = val;
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                            textField: TextFormField(
                              initialValue: _con.currentUser.licenseNo ?? "",
                              enabled: !_con.isLoading,
                              validator: (val) =>
                              (val.isEmpty) ? "License No must not be empty" : null,
                              style: textTheme.bodyText1.merge(
                                  TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "License No",
                                hintStyle: textTheme.bodyText1
                                    .merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(
                                  CupertinoIcons.news,
                                  color: config.Colors().textColor(1),
                                ),
                                prefixIconConstraints:
                                BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val) {
                                _con.currentUser.licenseNo = val;
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),

                          _con.categories.isNotEmpty
                              ? NeuTextFieldContainer(
                            borderRadius: BorderRadius.circular(15),
                            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                            textField: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              child: ListView.builder(
                                  primary: false,
                                  shrinkWrap: true,
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  itemCount: _con.categories.length,
                                  itemBuilder: (context, index) {
                                    Category category = _con.categories[index];
                                    return InkWell(
                                      onTap: (){
                                        if(!_con.isLoading) {
                                          setState((){
                                            _con.selectedCategory = category;
                                          });
                                        }
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(category.name, style: textTheme.bodyText1,),
                                            Spacer(),
                                            NeuBtnCheckBox(
                                              onPressed: (){
                                                if(!_con.isLoading) {
                                                  setState((){
                                                    _con.selectedCategory = category;
                                                  });
                                                }
                                              },
                                              state: _con.selectedCategory != null && _con.selectedCategory.id==category.id,
                                              borderRadius: 100,
                                              width: 30,
                                              height: 30,
                                              child: Icon(CupertinoIcons.check_mark, color: _con.selectedCategory != null && _con.selectedCategory.id==category.id ? theme.accentColor : Colors.white,),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                              ),
                            ),
                          )
                              : CircularLoadingWidget(height: 100)


                        ],
                      ),
                    ),

                    SizedBox(
                      height: 15,
                    ),

                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: _con.isLoading
                            ? NeuContainer(
                            width: 50,
                            height: 50,
                            borderRadius: 100,
                            child: CircularProgressIndicator())
                            : NeuButtonContainer(
                          onPressed: (){
                            _con.updateUserNow();
                          },
                          height: 50,
                          width: 300,
                          borderRadius: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(
                                Icons.person_pin,
                                color: theme.accentColor,
                              ),
                              Text(
                                "Update Profile",
                                style: textTheme.bodyText2
                                    .merge(TextStyle(fontSize: 16)),
                              ),
                              SizedBox(
                                width: 5,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),

              _con.showDialog
                  ? Container(
                width: size.width,
                height: size.height,
                color: config.Colors().scaffoldColor(0.5),
                child: Center(
                  child: NeuContainer(
                    width: 200,
                    height: 150,
                    borderRadius: 10,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Column(
                        children: [
                          Text("Add new Profile photo", style: textTheme.subtitle1),
                          SizedBox(height: 5,),
                          Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                          SizedBox(height: 5,),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("Choose how do you want to select new profile photo",
                              style: textTheme.bodyText2,),
                          ),

                          Spacer(),

                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              children: [
                                NeuButtonContainer(
                                  onPressed: (){
                                    _con.getCameraImage();
                                  },
                                  width: 80,
                                  height: 40,
                                  borderRadius: 10,
                                  child: Center(
                                    child: GradientIcon(CupertinoIcons.camera_fill, size: 22, gradient: config.Colors().iconGradient(),),
                                  ),
                                ),

                                Spacer(),

                                NeuButtonContainer(
                                  onPressed: (){
                                    _con.getGalleryImage();
                                  },
                                  width: 80,
                                  height: 40,
                                  borderRadius: 10,
                                  child: Center(
                                    child: GradientIcon(Icons.photo, size: 20, gradient: config.Colors().iconGradient(),),
                                  ),
                                )
                              ],
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                ),
              ) : SizedBox(height: 0, width: 0,),

            ],
          ),
        ),
      ),
    );
  }
}
