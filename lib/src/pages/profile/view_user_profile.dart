import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/strings.dart';
import 'package:nurses_app/src/controllers/user_profile_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/elements/job/job_list_item.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/neumorphic/neu_container.dart';

class ViewUserProfileWidget extends StatefulWidget {
  RouteArgument argument;

  ViewUserProfileWidget({this.argument});

  @override
  _ViewUserProfileWidgetState createState() => _ViewUserProfileWidgetState();
}

class _ViewUserProfileWidgetState extends StateMVC<ViewUserProfileWidget> {

  UserProfileController _con;

  _ViewUserProfileWidgetState() : super(UserProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.user = widget.argument.user;
    if(widget.argument.getUserDetails) {
      _con.getUserDetails();
    }
    _con.getCurrentUser();
    super.initState();
    _con.initLocationController();
    _con.getUserJobs();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: config.Colors().appBar(context, _con.user.name),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                CachedNetworkImage(
                  imageUrl: _con.user.image,
                  fit: BoxFit.cover,
                  imageBuilder: (context, provider) {
                      return Container(
                        height: 270,
                        width: size.width,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover
                          )
                        ),
                      );
                  },
                ),
                Container(
                  height: 270,
                  width: size.width,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        theme.scaffoldBackgroundColor.withOpacity(0),
                        theme.scaffoldBackgroundColor.withOpacity(0),
                        theme.scaffoldBackgroundColor.withOpacity(0.3),
                        theme.scaffoldBackgroundColor.withOpacity(0.7),
                        theme.scaffoldBackgroundColor.withOpacity(1),
                      ],
                      stops: [0, 0.3, 0.5, 0.7, 1],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter
                    )
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(_con.user.name, style: textTheme.headline3,),
                            SizedBox(height: 5,),
                            Text(_con.user.type.toString() == HOSPITAL_TYPE ? 'HOSPITAL' : "", style: textTheme.caption,),
                          ],
                        ),
                      ),

                      IconButton(
                          onPressed: (){
                            Navigator.of(context).pushNamed("/OpenChat", arguments: RouteArgument(user: _con.user, currentUser: _con.currentUser));
                          },
                        icon: Icon(CupertinoIcons.chat_bubble, color: theme.accentColor,),
                      )
                      
                    ],
                  ),
                  SizedBox(height: 20,),

                  NeuContainer(
                    width: size.width,
                    borderRadius: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                CupertinoIcons.location_solid,
                                color: theme.accentColor,
                              ),
                              SizedBox(width: 10),
                              Text(
                                "Location on Map",
                                style: textTheme.headline1.merge(TextStyle(fontSize: 18)),
                              ),
                            ],
                          ),

                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                            child: Container(
                              height: 130,
                              child: GoogleMap(
                                onMapCreated: (GoogleMapController controller) {
                                  _con.controller = controller;
                                  _con.controller.setMapStyle(_con.mapStyle);
                                  _con.moveCamera();
                                },
                                mapType: MapType.normal,
                                initialCameraPosition: _con.user != null
                                    ? CameraPosition(
                                    target: LatLng(
                                        _con.user.lat != null ?
                                        double.parse(_con.user.lat.toString()) : 40.7237765,
                                        _con.user.long != null ?
                                        double.parse(_con.user.long.toString()) : -74.017617), zoom: 13.0)
                                    : CameraPosition(
                                    target: LatLng(40.7237765, -74.017617), zoom: 13.0),
                                markers: Set.from(_con.allMarkers),
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 20,),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.ac_unit, color: theme.accentColor, size: 18,),
                      SizedBox(width: 5,),
                      Text("Jobs by this User", style: textTheme.headline5,),
                    ],
                  ),
                  SizedBox(height: 15,),

                  _con.jobs.isEmpty
                      ? CircularLoadingWidget(height: 100,)
                      : ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.jobs.length,
                    itemBuilder: (context, index) {
                      Job _job = _con.jobs[index];
                      return JobListItem(job: _job,
                          onProfileTap: (){
                          },
                          onTap: (){
                            Navigator.of(context).pushNamed("/ViewJobDetails", arguments: RouteArgument(job: _job));
                          });
                    },
                  ),

                  SizedBox(height: 20,)

                ],
              ),
            )
          ],
        ),
      ),
    );
  }


}
