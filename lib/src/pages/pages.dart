import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/controller.dart';
import 'package:nurses_app/src/neumorphic/neu_button_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart' as c;
import 'package:nurses_app/src/neumorphic/neu_container_btn_with_state.dart';
import 'package:nurses_app/src/pages/main_pages/chats_widget.dart';
import 'package:nurses_app/src/pages/main_pages/home.dart';
import 'package:nurses_app/src/pages/main_pages/location_widget.dart';
import 'package:nurses_app/src/pages/main_pages/profile_widget.dart';
import 'package:nurses_app/src/pages/main_pages/settings_widget.dart';

class PagesWidget extends StatefulWidget {
  int currentTab;
  // Widget currentPage = OrdersWidget();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesWidget({
    Key key,
    this.currentTab,
  }) {
    currentTab = currentTab != null ? currentTab : 1;
  }

  @override
  _PagesWidgetState createState() => _PagesWidgetState();
}

class _PagesWidgetState extends StateMVC<PagesWidget> {

  Controller _con;
  PageController _pageController;

  final List<Widget> _pages = [];

  _PagesWidgetState() : super(Controller()) {
    _con = controller;
  }

  @override
  initState() {
    super.initState();
    _pageController = PageController();
    setState(() {
      //_pages.add(HomePage());
      _pages.add(ProfileWidget());
      _pages.add(LocationWidget());
       _pages.add(HomePage());
      _pages.add(ChatsWidget());
      _pages.add(SettingsWidget());
    });
    Future.delayed(Duration.zero, () {
      _selectPage(widget.currentTab);
    });
    _con.initFCM();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onPageChanged(int index) {
    setState(() {
      widget.currentTab = index;
    });
  }

  void _selectPage(int index) {
    setState(() {
      widget.currentTab = index;
    });
    _pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget.scaffoldKey,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: PageView(
                children: _pages,
                controller: _pageController,
                onPageChanged: _onPageChanged,
                physics: NeverScrollableScrollPhysics()
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Theme(
                data: Theme.of(context)
                    .copyWith(canvasColor: Colors.transparent),
                child: Container(
                  height: 70,
                  decoration: BoxDecoration(
                      gradient:  config.Colors().bottomNavGradient()
                  ),
                  child: BottomNavigationBar(
                    showSelectedLabels: false,
                    showUnselectedLabels: false,
                    type: BottomNavigationBarType.fixed,
                    elevation: 0,
                    currentIndex: widget.currentTab,
                    backgroundColor: Colors.transparent,
                    selectedIconTheme: IconThemeData(size: 25, color: Theme.of(context).accentColor),
                    unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
                    onTap: _selectPage,
                    items: [
                      c.BottomNavigationBarItem(
                        title: Container(height: 0.0,),
                        icon: NeuBtnContainerWithState(
                          onPressed: (){},
                          state: widget.currentTab == 0,
                          child: Icon(c.CupertinoIcons.person),
                          width: 50,
                          height: 50,
                          borderRadius: 15,
                        ),
                      ),

                      c.BottomNavigationBarItem(
                        title: Container(height: 0.0,),
                        icon: NeuBtnContainerWithState(
                          onPressed: (){},
                          state: widget.currentTab == 1,
                          child: Icon(c.CupertinoIcons.location),
                          width: 50,
                          height: 50,
                          borderRadius: 15,

                        ),
                      ),

                      c.BottomNavigationBarItem(
                        title: Container(height: 0.0,),
                        icon: NeuBtnContainerWithState(
                          onPressed: (){},
                          state: widget.currentTab == 2,
                          child: Icon(c.CupertinoIcons.home),
                          width: 50,
                          height: 50,
                          borderRadius: 15,

                        ),
                      ),

                      c.BottomNavigationBarItem(
                        title: Container(height: 0.0,),
                        icon: NeuBtnContainerWithState(
                          onPressed: (){},
                          state: widget.currentTab == 3,
                          child: Icon(c.CupertinoIcons.conversation_bubble),
                          width: 50,
                          height: 50,
                          borderRadius: 15,

                        ),
                      ),

                      c.BottomNavigationBarItem(
                        title: Container(height: 0.0,),
                        icon: NeuBtnContainerWithState(
                          onPressed: (){},
                          state: widget.currentTab == 4,
                          child: Icon(c.CupertinoIcons.gear),
                          width: 50,
                          height: 50,
                          borderRadius: 15,

                        ),
                      ),

                    ],
                  ),
                ),)
          )
        ],
      ),

    );
  }


  Widget _appbar(){
    return AppBar(
      leading: NeuButtonContainer(
        onPressed: (){},
        height: 50,
        width: 50,
        borderRadius: 100,
        child: c.Icon(c.CupertinoIcons.home),
      ),
    );
  }

}
