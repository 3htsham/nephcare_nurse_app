import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/job_controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_horizontal_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class ViewJobWidget extends StatefulWidget {
  RouteArgument argument;

  ViewJobWidget({this.argument});

  @override
  _ViewJobWidgetState createState() => _ViewJobWidgetState();
}

class _ViewJobWidgetState extends StateMVC<ViewJobWidget> {

  JobController _con;

  _ViewJobWidgetState() : super(JobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.job = widget.argument.job;
    super.initState();
    _con.getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    

    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(_con.job.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: Container(
        height: size.height,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Stack(
          children: [

            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.arrow_right, color: theme.accentColor),
                        Text(_con.job.title, style: textTheme.headline3),
                      ],
                    ),
                    SizedBox(height: 10),
                    
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Spacer(),
                        Row(
                          children: [
                            Icon(CupertinoIcons.clock, size: 16, color: config.Colors().textColor(1),),
                            SizedBox(width: 5,),
                            Text('${_con.job.hours ?? 0} hours', style: textTheme.caption,)
                          ],
                        ),
                        SizedBox(width: 25,),
                        Row(
                          children: [
                            Icon(Icons.attach_money, size: 16, color: config.Colors().textColor(1),),
                            SizedBox(width: 5,),
                            Text(_con.job.pay, style: textTheme.caption,)
                          ],
                        ),
                        SizedBox(width: 10,),
                      ],
                    ),
                    SizedBox(height: 5),
                
                    InkWell(
                      onTap: (){
                        Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _con.job.user));
                      },
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: _con.job.user != null && _con.job.user.image != null && _con.job.user.image.length > 0
                              ? FadeInImage.assetNetwork(
                                placeholder: "assets/img/placeholders/profile_placeholder.png",
                                image: _con.job.user.image,
                                width: 40,
                                height: 40,
                                fit: BoxFit.cover,
                              )
                            : Image.asset(
                                "assets/img/placeholders/profile_placeholder.png",
                                width: 40,
                                height: 40,
                                fit: BoxFit.cover,
                              )
                          ),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text(_con.job.user.name ?? "Job Poster",
                                  style: textTheme.bodyText2.merge(TextStyle(fontSize: 12)),
                                )
                              ),
                              SizedBox(height: 5),
                              Container(
                                child: Text('Posted on $day at $time', style: textTheme.caption.merge(TextStyle(fontSize: 10)))),
                            ],
                          ),
                        ]
                      ),
                    ),

                    SizedBox(height: 10),
                    Text('Job Details', style: textTheme.headline4),
                    SizedBox(height: 10),

                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(_con.job.description ?? "", style: textTheme.bodyText1)),

                    SizedBox(height: 80)
                  ],
                ),
              ),
            ),

            Positioned(
              bottom: 0,
              child: Container(
                width: size.width,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                      InkWell(
                        onTap: (){
                          Navigator.of(context).pushNamed("/OpenChat", 
                            arguments: RouteArgument(currentUser: _con.currentUser, 
                                                      user: _con.job.user));
                        },
                          child: NeuHorizontalContainer(
                          height: 50,
                          width: 150,
                          borderRadius: 100,
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(CupertinoIcons.chat_bubble, color: theme.accentColor,),
                                SizedBox(width: 5,),
                                Text("Contact", style: textTheme.subtitle1,)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10,),
                    _con.isLoading
                          ? NeuContainer(
                              width: 50,
                              height: 50,
                              borderRadius: 100,
                              child: CircularProgressIndicator())
                          : InkWell(
                              onTap: () {
                                if(!(_con.job.applied == true) && !(_con.job.applied == "true")) {
                                  _con.applyOnJob();
                                }
                              },
                              child: NeuHorizontalContainer(
                                height: 50,
                                width: 150,
                                borderRadius: 100,
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        (_con.job.applied == true) || (_con.job.applied == "true")
                                            ? CupertinoIcons.checkmark_circle_fill
                                            : CupertinoIcons.check_mark_circled,
                                        color: theme.accentColor,
                                      ),
                                      SizedBox(width: 5,),
                                      Text(
                                        (_con.job.applied == true) || (_con.job.applied == "true")
                                            ? "Applied"
                                            :"Apply",
                                        style: textTheme.subtitle1,)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                    ],
                ),
              ) 
            ),
          ],
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Job Details",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor), 
        onPressed: (){
            Navigator.of(context).pop();
          }
        ),
      actions: [
      ],
    );
  }

}