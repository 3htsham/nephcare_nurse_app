import 'package:nurses_app/src/controllers/settings_controller.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends StateMVC<SettingsWidget>
    with AutomaticKeepAliveClientMixin  {

  SettingsController _con;

  _SettingsWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getCurrentUser();
    _con.getNotificationStatus();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Settings", style: textTheme.headline5,),
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: NeuContainer(
                width: size.width,
                height: 300,
                borderRadius: 10,
                child: Column(
                  children: [
                    ListTile(
                      title: Text("Notifications", style: textTheme.headline5,),
                      subtitle: Text("If you turn them off, you will not be notified for any chat message or hire request."),
                      leading: GradientIcon(CupertinoIcons.bell_solid, size: 22, gradient: config.Colors().iconGradient(),),
                      trailing: InkWell(
                        onTap: _con.switchNotifications,
                        child: NeuTextFieldContainer(
                          textField: AnimatedContainer(
                            duration: Duration(milliseconds: 600),
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                              color: _con.isNotificationsOn ? theme.accentColor : theme.scaffoldBackgroundColor,
                              borderRadius: BorderRadius.circular(100),
                              boxShadow: [
                                BoxShadow(
                                  color: config.Colors().shadowLight,
                                  blurRadius: 3,
                                  offset: Offset(-0.5, 0.5)
                                ),
                                BoxShadow(
                                    color: config.Colors().shadowDark,
                                    blurRadius: 3,
                                    offset: Offset(0.5, -0.5),
                                ),
                              ]
                            ),
                            margin: EdgeInsets.only(top: 3, bottom: 3, right: _con.isNotificationsOn ? 3 : 20, left: _con.isNotificationsOn ? 20 : 3),
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text("Tell your friends", style: textTheme.headline5,),
                      leading: GradientIcon(CupertinoIcons.group_solid, size: 22, gradient: config.Colors().iconGradient(),),
                    ),
                    ListTile(
                      title: Text("About us", style: textTheme.headline5,),
                      leading: GradientIcon(Icons.info, size: 22, gradient: config.Colors().iconGradient(),),
                      onTap: (){
                        Navigator.of(context).pushNamed("/About");
                      },
                    ),
                    ListTile(
                      title: Text("Contact Support", style: textTheme.headline5,),
                      leading: GradientIcon(Icons.chat_bubble, size: 22, gradient: config.Colors().iconGradient(),),
                      onTap: (){
                        Navigator.of(context).pushNamed("/Contact");
                      },
                    ),
                    ListTile(
                      title: Text("Version", style: textTheme.headline5,),
                      leading: GradientIcon(Icons.play_arrow, size: 22, gradient: config.Colors().iconGradient(),),
                      trailing: Text("v1.0", style: textTheme.caption,),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
