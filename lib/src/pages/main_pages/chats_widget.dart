import 'package:nurses_app/src/controllers/chat_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/elements/chats_list_item.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class ChatsWidget extends StatefulWidget {
  @override
  _ChatsWidgetState createState() => _ChatsWidgetState();
}

class _ChatsWidgetState extends StateMVC<ChatsWidget> {

  ChatController _con;

  _ChatsWidgetState() : super(ChatController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Chats", style: textTheme.headline3,),
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10, right: 25, left: 25),
              child: NeuTextFieldContainer(
                padding: EdgeInsets.symmetric(horizontal: 5),
                textField: TextFormField(
                  obscureText: true,
                  style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Search",
                    hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                    prefixIcon: Icon(CupertinoIcons.search, color: theme.focusColor,),
                    prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                  ),
                  onSaved: (val){
                    // email = val;
                  },
                ),
              ),
            ),
            SizedBox(height: 10,),


            _con.chats.isEmpty
            ? Center(
              child: Opacity(
                opacity: 0.2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Icon(Icons.chat_bubble, size: 40, color: theme.focusColor),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "You don't have any chats",
                      style: textTheme.bodyText1,
                    )
                  ],
                ),
              ),
            )
            : ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: _con.chats.length,
              itemBuilder: (ctx, index) {
                return ChatsListItem(chat: _con.chats[index], 
                  onTap: (){
                    Navigator.of(context).pushNamed("/OpenChat", 
                        arguments: RouteArgument(currentUser: _con.currentUser, chat: _con.chats[index]));
                  },
                );
              }
            ),

            SizedBox(height: 60,),

          ],
        ),
      ),
    );
  }
}
