import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/generated/i18n.dart';
import 'package:nurses_app/src/controllers/home_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/elements/job/job_list_item.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button_container.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage>
    with AutomaticKeepAliveClientMixin {

  HomeController _con;
  
  _HomePageState() : super(HomeController()) {
    _con = controller;
  }

  @override
  initState(){
    _con.currentUser = userRepo.currentUser;
    _con.getJobs();
    _con.getMyHireRequests();
    _con.getMyOnGoingJobs();
    _con.getMyInstantHireRequests();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return RefreshIndicator(
      onRefresh: () async {},
        child: Scaffold(
          key: _con.scaffoldKey,
          appBar: _appbar(),
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [

                _con.trackId == null
                    ? SizedBox(height: 0)
                    : NeuButtonContainer(
                  onPressed: (){
                    Navigator.of(context).pushNamed("/trackLocation", arguments: RouteArgument(trackingReqId: _con.trackId));
                  },
                  borderRadius: 10,
                  height: 50,
                  width: size.width - 20,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.watch_later_outlined, color: theme.accentColor, size: 16,),
                        SizedBox(width: 5),
                        Text("You've unfinished job tracking", style: textTheme.subtitle1,),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: _con.trackId == null ? 0 : 15,),

                _con.instantRequests.isEmpty
                    ? SizedBox(height: 0)
                    : NeuButtonContainer(
                  onPressed: (){
                    Navigator.of(context).pushNamed("/MyHireRequests", arguments: RouteArgument(currentUser: _con.currentUser));
                  },
                  borderRadius: 10,
                  height: 50,
                  width: size.width - 20,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.star, color: theme.accentColor, size: 16,),
                        SizedBox(width: 5),
                        Text("There are some Instant Requests", style: textTheme.subtitle1,),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: _con.instantRequests.isEmpty ? 0 : 15,),

                _con.requests.isEmpty
                    ? SizedBox(height: 0)
                    : NeuButtonContainer(
                  onPressed: (){
                    Navigator.of(context).pushNamed("/MyHireRequests", arguments: RouteArgument(currentUser: _con.currentUser));
                  },
                  borderRadius: 10,
                  height: 50,
                  width: size.width - 20,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.star, color: theme.accentColor, size: 16,),
                        SizedBox(width: 5),
                        Text("You have new hire Requests", style: textTheme.subtitle1,),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: _con.requests.isEmpty ? 0 : 15,),

                _con.jobTracks.isEmpty
                    ? SizedBox(height: 0)
                    : NeuButtonContainer(
                  onPressed: (){
                    Navigator.of(context).pushNamed("/ViewOnGoingJobsList",
                        arguments: RouteArgument(currentUser: _con.currentUser));
                  },
                  borderRadius: 10,
                  height: 50,
                  width: size.width - 20,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.person_pin, color: theme.accentColor, size: 16,),
                        SizedBox(width: 5),
                        Text("Tap to see ongoing Jobs", style: textTheme.subtitle1,),
                      ],
                    ),
                  ),
                ),


                _con.jobs.isEmpty 
                ? CircularLoadingWidget(height: 100)
                : ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  itemCount: _con.jobs.length,
                  itemBuilder: (context, index){
                    Job job = _con.jobs[index];
                    return JobListItem(job: job,
                        onProfileTap: (){
                          Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _con.jobs[index].user));
                        },
                        onTap: (){
                      Navigator.of(context).pushNamed("/ViewJobDetails", arguments: RouteArgument(job: job));
                    });
                  }
                ),

                SizedBox(height: 60,)


              ],
            ),
          ),
        )
    );
  }


  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).home,
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      automaticallyImplyLeading: false,
      actions: [
      ],
    );
  }

}











