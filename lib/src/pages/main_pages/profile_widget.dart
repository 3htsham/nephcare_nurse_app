import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/profile/email_profile_widget.dart';
import 'package:nurses_app/src/elements/profile/location_profile_widget.dart';
import 'package:nurses_app/src/elements/profile/phone_profile_widget.dart';
import 'package:nurses_app/src/elements/profile/qualification_widget.dart';
import 'package:nurses_app/src/elements/profile/specialization_widget_item.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget> {

  UserController _con;

  _ProfileWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.initLocationController();
    super.initState();
    _con.getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    // var month = DateFormat('MMMM').format(_con.currentUser.createdOn);
    // var year = DateFormat('yyyy').format(_con.currentUser.createdOn);

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Account", style: textTheme.headline5,),
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
              child: Row(
                children: [
                  NeuTextFieldContainer(
                    padding: EdgeInsets.all(7),
                    textField: NeuContainer(
                      borderRadius: 100,
                      width: 70,
                      height: 70,
                      child: Padding(
                        padding: EdgeInsets.all(3),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: _con.currentUser != null && _con.currentUser.image != null 
                          ? FadeInImage.assetNetwork(
                              placeholder: "assets/img/placeholders/profile_placeholder.png",
                              image: _con.currentUser.image,
                            width: 70,
                            height: 70,
                            fit: BoxFit.cover,
                          ) : Image.asset(
                              "assets/img/placeholders/profile_placeholder.png",
                              width: 70,
                              height: 70,
                              fit: BoxFit.cover,
                            ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(width: 100, child: Text(_con.currentUser != null ? _con.currentUser.name ?? "" : "", style: textTheme.headline6, maxLines: 1, overflow: TextOverflow.ellipsis,)),
                        Text(
                          _con.currentUser != null
                              ? _con.currentUser.category != null
                              ? _con.currentUser.category.name ?? ""
                              : ""
                              : "",
                          style: textTheme.caption,)
                      ],
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.of(context).pushNamed("/EditProfile", arguments: RouteArgument(currentUser: _con.currentUser))
                          .then((value) {
                        if(value != null) {
                          if(value) {
                            _con.getCurrentUser();
                          }
                        }
                      });
                    },
                    icon: Icon(CupertinoIcons.pencil, color: theme.accentColor,),
                  ),
                ],
              ),
            ),

            _con.currentUser != null
            ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: NeuContainer(
                width: size.width,
                height: 55,
                borderRadius: 10,
                child: ListTile(
                  title: Text("My Wallet", style: textTheme.headline5,),
                  leading: GradientIcon(
                    Icons.account_balance_wallet,
                    size: 22,
                    gradient: config.Colors().iconGradient(),
                  ),
                    trailing: Icon(
                      CupertinoIcons.forward,
                      size: 18,
                      color: theme.focusColor,
                    ),
                  onTap: (){
                    Navigator.of(context).pushNamed("/MyWallet",
                        arguments:
                        RouteArgument(currentUser: _con.currentUser));
                  }
                ),
              ),
            )
            : SizedBox(height: 0),

            SizedBox(height: 10),
             
            _con.currentUser != null 
            ? EmailProfileidget(con: _con) 
            : SizedBox(height: 0),

            _con.currentUser != null
                ? PhoneProfileWidget(con: _con)
                : SizedBox(height: 0),

            _con.currentUser != null
                ? QualificationProfileWidget(
                    con: _con,
                    onTap: () {
                      Navigator.of(context).pushNamed("/EditQualifications", arguments: RouteArgument(currentUser: _con.currentUser))
                          .then((value) {
                        if(value != null) {
                          if(value) {
                            _con.getCurrentUser();
                          }
                        }
                      });
                    },
                  )
                : SizedBox(height: 0),

            _con.currentUser != null
                ? SpecializationWidgetItem(
                    con: _con,
                    onTap: () {
                      Navigator.of(context).pushNamed("/EditSpecifications", arguments: RouteArgument(currentUser: _con.currentUser))
                          .then((value) {
                        if(value != null) {
                          if(value) {
                            _con.getCurrentUser();
                          }
                        }
                      });
                    },
                  )
                : SizedBox(height: 0),
            AddressProfileWidget(con: _con, onTap: (){
              Navigator.of(context).pushNamed("/EditMyLocation",
                  arguments: RouteArgument(currentUser: _con.currentUser)).then((value) {
                if(value != null) {
                  if(value) {
                    _con.getCurrentUser();
                  }
                }
              });
            },),
            
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: NeuContainer(
                width: size.width,
                borderRadius: 10,
                child: Column(
                  children: [

                    ListTile(
                      onTap: (){
                        userRepo.logout().then((value) {
                          Navigator.of(context).pushNamedAndRemoveUntil("/Login", (Route<dynamic> route) => false);
                        });
                      },
                      title: Text("Logout", style: textTheme.headline5,),
                      leading: GradientIcon(Icons.power_settings_new, size: 22, gradient: config.Colors().iconGradient(),),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(height: 60,)

          ],
        ),
      ),
    );
  }
}
