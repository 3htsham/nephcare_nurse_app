import 'package:nurses_app/config/strings.dart';
import 'package:nurses_app/src/controllers/hire_controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_button_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart';

class HireProposalDetails extends StatefulWidget {
  RouteArgument argument;
  HireProposalDetails({this.argument});

  @override
  _HireProposalDetailsState createState() => _HireProposalDetailsState();
}

class _HireProposalDetailsState extends StateMVC<HireProposalDetails> {

  HireController _con;

  _HireProposalDetailsState() : super(HireController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.request = widget.argument.request;
    _con.getUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _con.request.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);


    var scheduleWeekDay = "";
    var scheduleDate = "";
    var scheduleTime = "";

    if(_con.request.isScheduled) {
      var schedule = _con.request.schedule;
      scheduleWeekDay = DateFormat('EEEE').format(schedule);
      scheduleDate = DateFormat('dd MMM, yyyy').format(schedule);
      scheduleTime = DateFormat('hh:mm aa').format(schedule);
    }

    var timeNow = DateTime.now().millisecondsSinceEpoch;

    // _con.isLoading = false;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        } else if(_con.showAccept) {
          setState((){ _con.showAccept = false; });
          return false;
        } else if(_con.showWithdraw) {
          setState((){ _con.showWithdraw = false; });
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 7),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(width: 0.5, color: theme.accentColor)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text("Sent by: ", style: textTheme.headline5,),
                              SizedBox(width: 50),
                              InkWell(
                                onTap: (){
                                  Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _con.request.sender, getUserDetails: true));
                                },
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: FadeInImage.assetNetwork(
                                        placeholder: "assets/img/placeholders/profile_placeholder.png",
                                        image: _con.request.sender.image,
                                        width: 40,
                                        height: 40,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    SizedBox(width: 10,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(_con.request.sender.name, style: textTheme.subtitle1,),
                                        Text(_con.request.sender.type.toString() == HOSPITAL_TYPE ? "Hospital" : "",
                                          style: textTheme.caption,)
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),


                          SizedBox(height: 10,),

                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Budget offered: ", style: textTheme.headline5),
                              Text('\$${_con.request.pay.toString()}', style: textTheme.bodyText1,)
                            ],
                          ),

                          SizedBox(height: 10,),

                          _con.request.job != null ?
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("For Job", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text(_con.request.job.title, style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.request.job.description,
                                  style: textTheme.bodyText2),

                            ],
                          ) : SizedBox(height: 0),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              SizedBox(height: 10,),

                              Container(height: 0.3, width: size.width, color: theme.focusColor,),

                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.message, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text("More Details...", style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.request.description,
                                  style: textTheme.bodyText2),

                              SizedBox(height: 5,),

                              Container(height: 0.3, width: size.width, color: theme.focusColor,),

                              SizedBox(height: 10,),
                              Text('on $day at $time', style: textTheme.caption,),

                              SizedBox(height: 5,),
                            ],
                          ),



                          SizedBox(height: _con.request.isScheduled ? 10 : 0,),
                          _con.request.isScheduled
                              ? Row(
                            children: [
                              Icon(CupertinoIcons.clock_solid, color: theme.accentColor, size: 14,),
                              SizedBox(width: 10),
                              Text('Scheduled ', style: textTheme.caption.merge(TextStyle(color: theme.accentColor)),),
                              Text(' on $scheduleWeekDay $scheduleDate at $scheduleTime', style: textTheme.caption,),
                            ],
                          )
                              : SizedBox(height: 0, width: 0),

                        ],
                      ),
                    ),

                    SizedBox(height: 15),


                    _con.isLoading
                        ? NeuButtonContainer(
                            onPressed: () {},
                            width: 50,
                            height: 50,
                            borderRadius: 100,
                            child: CircularProgressIndicator(),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [


                              NeuButtonContainer(
                                onPressed: () {
                                  if(!_con.request.isScheduled || timeNow >= _con.request.schedule.millisecondsSinceEpoch) {
                                    setState(() {
                                      _con.showAccept = true;
                                    });
                                  } else {
                                    showToast("Scheduled request can't be accepted before time");
                                  }
                                },
                                width: 150,
                                height: 50,
                                borderRadius: 100,
                                child: Center(
                                  child: Text(
                                    "ACCEPT",
                                    style: textTheme.subtitle1,
                                  ),
                                ),
                              ),

                              SizedBox(width: 15),

                              NeuButtonContainer( 
                                onPressed: () {
                                  setState((){
                                    _con.showWithdraw = true;
                                  });
                                },
                                width: 150,
                                height: 50,
                                borderRadius: 100,
                                child: Center(
                                  child: Text(
                                    "REJECT",
                                    style: textTheme.subtitle1,
                                  ),
                                ),
                              ),

                            ],
                          )
                  ],
                ),
              ),
            ),

            _con.showAccept
                ? Container(
              width: size.width,
              height: size.height,
              color: config.Colors().scaffoldColor(0.5),
              child: Center(
                child: NeuContainer(
                  width: 230,
                  height: 190,
                  borderRadius: 10,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Column(
                      children: [
                        Text("Hire Request", style: textTheme.subtitle1),
                        SizedBox(height: 5,),
                        Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                        SizedBox(height: 5,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Are you sure you want to accept this request?\n\nOnly accept if you're ready to go.\nLive location tracking will be started after accepting request ",
                            style: textTheme.bodyText2,),
                        ),

                        Spacer(),

                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            children: [

                              NeuButtonContainer(
                                onPressed: (){
                                  _con.acceptRequest();
                                },
                                width: 80,
                                height: 40,
                                borderRadius: 10,
                                child: Center(
                                  child: GradientIcon(CupertinoIcons.checkmark_circle_fill, size: 22, gradient: config.Colors().iconGradient(),),
                                ),
                              ),

                              Spacer(),

                              NeuButtonContainer(
                                onPressed: (){
                                  setState((){
                                    _con.showAccept = false;
                                  });
                                },
                                width: 80,
                                height: 40,
                                borderRadius: 10,
                                child: Center(
                                  child: GradientIcon(CupertinoIcons.clear_circled_solid, size: 20, gradient: config.Colors().iconGradient(),),
                                ),
                              )
                            ],
                          ),
                        )

                      ],
                    ),
                  ),
                ),
              ),
            ) : SizedBox(height: 0, width: 0,),

            _con.showWithdraw
                ? Container(
              width: size.width,
              height: size.height,
              color: config.Colors().scaffoldColor(0.5),
              child: Center(
                child: NeuContainer(
                  width: 230,
                  height: 150,
                  borderRadius: 10,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Column(
                      children: [
                        Text("Reject Request", style: textTheme.subtitle1),
                        SizedBox(height: 5,),
                        Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                        SizedBox(height: 5,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Are you sure you want to reject this request?",
                            style: textTheme.bodyText2,),
                        ),

                        Spacer(),

                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            children: [
                              NeuButtonContainer(
                                onPressed: (){
                                  _con.withdrawRequest();
                                },
                                width: 80,
                                height: 40,
                                borderRadius: 10,
                                child: Center(
                                  child: GradientIcon(CupertinoIcons.checkmark_circle_fill, size: 22, gradient: config.Colors().iconGradient(),),
                                ),
                              ),

                              Spacer(),

                              NeuButtonContainer(
                                onPressed: (){
                                  // _con.getGalleryImage();
                                  setState((){_con.showWithdraw = false;});
                                },
                                width: 80,
                                height: 40,
                                borderRadius: 10,
                                child: Center(
                                  child: GradientIcon(CupertinoIcons.clear_circled_solid, size: 20, gradient: config.Colors().iconGradient(),),
                                ),
                              )
                            ],
                          ),
                        )

                      ],
                    ),
                  ),
                ),
              ),
            ) : SizedBox(height: 0, width: 0,),
          ],
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Request Details",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
