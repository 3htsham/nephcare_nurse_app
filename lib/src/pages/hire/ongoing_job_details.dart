import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/ongoing_jobs_controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';

class OngoingJobDetailsWidget extends StatefulWidget {
  RouteArgument argument;

  OngoingJobDetailsWidget({this.argument});

  @override
  _OngoingJobDetailsWidgetWidgetState createState() => _OngoingJobDetailsWidgetWidgetState();
}

class _OngoingJobDetailsWidgetWidgetState extends StateMVC<OngoingJobDetailsWidget> {

  OnGoingJobController _con;

  _OngoingJobDetailsWidgetWidgetState() : super(OnGoingJobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.jobToTrack = widget.argument.jobToTrack;
    _con.getJobToTrackDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [

            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("OnGoing Jobs",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
