import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/config/strings.dart';
import 'package:nurses_app/src/controllers/hire_controller.dart';
import 'package:nurses_app/src/elements/CircularLoadingWidget.dart';
import 'package:nurses_app/src/models/hire_request.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button_container.dart';

class MyHireProposalsWidget extends StatefulWidget {
  RouteArgument argument;

  MyHireProposalsWidget({this.argument});

  @override
  _MyHireProposalsWidgetState createState() => _MyHireProposalsWidgetState();
}

class _MyHireProposalsWidgetState extends StateMVC<MyHireProposalsWidget> {

  HireController _con;

  _MyHireProposalsWidgetState() : super(HireController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getMyHireRequests();
    _con.getMyInstantHireRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              _con.instantRequests.isEmpty
                  ? SizedBox(height: 0)
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: _con.instantRequests.length,
                itemBuilder: (context, index) {
                  final _req = _con.instantRequests[index];

                  return _buildInstantRequestItem(_req);
                },
              ),

              _con.requests.isEmpty
                  ? CircularLoadingWidget(height: 100)
                  : ListView.builder(
                shrinkWrap: true,
                  primary: false,
                itemCount: _con.requests.length,
                itemBuilder: (context, index) {
                  final _req = _con.requests[index];

                  // var dateTime = _con.requests[index].time;
                  // var day = DateFormat('EEEE').format(dateTime);
                  // var time = DateFormat('hh:mm aa').format(dateTime);
                  //
                  // var scheduleWeekDay = "";
                  // var scheduleDate = "";
                  // var scheduleTime = "";
                  //
                  // if(_con.requests[index].isScheduled) {
                  //   var schedule = _con.requests[index].schedule;
                  //   scheduleWeekDay = DateFormat('EEEE').format(schedule);
                  //   scheduleDate = DateFormat('dd MMM, yyyy').format(schedule);
                  //   scheduleTime = DateFormat('hh:mm aa').format(schedule);
                  // }


                  /*
                  return InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed("/HireRequestDetails", arguments: RouteArgument(currentUser: _con.currentUser, request: _con.requests[index]));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 7),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(width: 0.5, color: theme.accentColor)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text("From: ", style: textTheme.headline5,),
                              SizedBox(width: 50),
                              InkWell(
                                onTap: (){
                                  Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _con.requests[index].sender, getUserDetails: true));
                                },
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: FadeInImage.assetNetwork(
                                          placeholder: "assets/img/placeholders/profile_placeholder.png",
                                          image: _con.requests[index].sender.image,
                                        width: 50,
                                        height: 50,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    SizedBox(width: 10,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(_con.requests[index].sender.name, style: textTheme.subtitle1,),
                                        Text(_con.requests[index].sender.type.toString() == HOSPITAL_TYPE ? "Hospital" : "",
                                          style: textTheme.caption,)
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: 10,),

                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Budget offered: ", style: textTheme.headline5),
                              Text('\$${_con.requests[index].pay.toString()}', style: textTheme.bodyText1,)
                            ],
                          ),

                          SizedBox(height: 10,),

                          _con.requests[index].job != null ?
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("For Job", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text(_con.requests[index].job.title, style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.requests[index].job.description,
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: textTheme.bodyText2),
                            ],
                          )
                          :
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Details", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text("Message:", style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.requests[index].description,
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: textTheme.bodyText2),
                            ],
                          ),

                          SizedBox(height: 10,),
                          Text('Requested on $day at $time', style: textTheme.caption,),



                          SizedBox(height: _con.requests[index].isScheduled ? 10 : 0,),
                          _con.requests[index].isScheduled
                              ? Row(
                            children: [
                              Icon(CupertinoIcons.clock_solid, color: theme.accentColor, size: 14,),
                              SizedBox(width: 10),
                              Text('Scheduled ', style: textTheme.caption.merge(TextStyle(color: theme.accentColor)),),
                              Text(' on $scheduleWeekDay $scheduleDate at $scheduleTime', style: textTheme.caption,),
                            ],
                          )
                          : SizedBox(height: 0, width: 0),
                          
                        ],
                      ),
                    ),
                  );
                  */

                  return _buildNormalRequestItem(_req);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNormalRequestItem(HireRequest _req) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _req.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    var scheduleWeekDay = "";
    var scheduleDate = "";
    var scheduleTime = "";

    if(_req.isScheduled) {
      var schedule = _req.schedule;
      scheduleWeekDay = DateFormat('EEEE').format(schedule);
      scheduleDate = DateFormat('dd MMM, yyyy').format(schedule);
      scheduleTime = DateFormat('hh:mm aa').format(schedule);
    }

    return InkWell(
      onTap: (){
        Navigator.of(context).pushNamed("/HireRequestDetails", arguments: RouteArgument(currentUser: _con.currentUser, request: _req));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 0.5, color: theme.accentColor)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("From: ", style: textTheme.headline5,),
                SizedBox(width: 50),
                InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _req.sender, getUserDetails: true));
                  },
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: FadeInImage.assetNetwork(
                          placeholder: "assets/img/placeholders/profile_placeholder.png",
                          image: _req.sender.image,
                          width: 50,
                          height: 50,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: 10,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(_req.sender.name, style: textTheme.subtitle1,),
                          Text(_req.sender.type.toString() == HOSPITAL_TYPE ? "Hospital" : "",
                            style: textTheme.caption,)
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),

            SizedBox(height: 10,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Budget offered: ", style: textTheme.headline5),
                Text('\$${_req.pay.toString()}', style: textTheme.bodyText1,)
              ],
            ),

            SizedBox(height: 10,),

            _req.job != null ?
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("For Job", style: textTheme.headline5),
                SizedBox(height: 5,),
                Container(height: 0.3, width: size.width, color: theme.focusColor,),
                SizedBox(height: 5,),
                Row(
                  children: [
                    Icon(Icons.arrow_right, color: theme.accentColor,),
                    SizedBox(width: 5,),
                    Text(_req.job.title, style: textTheme.subtitle1,),
                  ],
                ),
                SizedBox(height: 5,),
                Text(_req.job.description,
                    maxLines: 2,
                    overflow: TextOverflow.fade,
                    style: textTheme.bodyText2),
              ],
            )
                :
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Details", style: textTheme.headline5),
                SizedBox(height: 5,),
                Container(height: 0.3, width: size.width, color: theme.focusColor,),
                SizedBox(height: 5,),
                Row(
                  children: [
                    Icon(Icons.arrow_right, color: theme.accentColor,),
                    SizedBox(width: 5,),
                    Text("Message:", style: textTheme.subtitle1,),
                  ],
                ),
                SizedBox(height: 5,),
                Text(_req.description,
                    maxLines: 2,
                    overflow: TextOverflow.fade,
                    style: textTheme.bodyText2),
              ],
            ),

            SizedBox(height: 10,),
            Text('Requested on $day at $time', style: textTheme.caption,),



            SizedBox(height: _req.isScheduled ? 10 : 0,),
            _req.isScheduled
                ? Row(
              children: [
                Icon(CupertinoIcons.clock_solid, color: theme.accentColor, size: 14,),
                SizedBox(width: 10),
                Text('Scheduled ', style: textTheme.caption.merge(TextStyle(color: theme.accentColor)),),
                Text(' on $scheduleWeekDay $scheduleDate at $scheduleTime', style: textTheme.caption,),
              ],
            )
                : SizedBox(height: 0, width: 0),

          ],
        ),
      ),
    );

  }

  Widget _buildInstantRequestItem(HireRequest _req) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _req.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(width: 0.5, color: theme.accentColor)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(Icons.stream, color: theme.accentColor,),
              SizedBox(width: 10),
              Text("Instantly looking for ", style: textTheme.headline5,),
              Spacer(),
              Text("${_req.categoryName ?? ''}", style: textTheme.caption,),
            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              SizedBox(height: 10,),
              Text("From: ", style: textTheme.headline5,),
              SizedBox(width: 50),
              InkWell(
                onTap: (){
                  Navigator.of(context).pushNamed("/VieUserProfile", arguments: RouteArgument(user: _req.sender, getUserDetails: true));
                },
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/img/placeholders/profile_placeholder.png",
                        image: _req.sender.image,
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(_req.sender.name, style: textTheme.subtitle1,),
                        Text(_req.sender.type.toString() == HOSPITAL_TYPE ? "Hospital" : "",
                          style: textTheme.caption,)
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),

          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Requested on $day at $time', style: textTheme.caption,),

              NeuButtonContainer(
                borderRadius: 100,
                height: 30,
                width: 100,
                onPressed: (){
                  //TODO: Accept Hire Request Here
                  _con.acceptInstantRequest(_req);
                },
                child: Text("Accept", style: textTheme.bodyText2.merge(TextStyle(color: theme.accentColor)),),
              )
            ],
          ),

          SizedBox(height: 10,),

        ],
      ),
    );

  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Hire Requests",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
