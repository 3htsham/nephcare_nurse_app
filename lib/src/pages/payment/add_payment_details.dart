import 'package:nurses_app/config/card_utils.dart';
import 'package:nurses_app/src/elements/other/card_month_formatter.dart';
import 'package:nurses_app/src/elements/other/card_number_formatter.dart';
import 'package:nurses_app/src/models/card_type.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/neumorphic/neu_button.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/wallet_controller.dart';
import 'package:nurses_app/config/app_config.dart' as config;


class AddPaymentMethodWidget extends StatefulWidget {
  RouteArgument argument;
  AddPaymentMethodWidget({this.argument});

  @override
  _AddPaymentMethodWidgetState createState() => _AddPaymentMethodWidgetState();
}

class _AddPaymentMethodWidgetState extends StateMVC<AddPaymentMethodWidget> {

  WalletController _con;

  _AddPaymentMethodWidgetState() : super(WalletController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.cardType = CardType.Others;
    super.initState();
    _con.cardNumberController.addListener(_getCardTypeFrmNumber);
  }

  void _getCardTypeFrmNumber() {
    String input = CardUtils.getCleanedNumber(_con.cardNumberController.text);
    CardType cardType = CardUtils.getCardTypeFrmNumber(input);
    setState(() {
      _con.cardType = cardType;
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    _con.cardNumberController.removeListener(_getCardTypeFrmNumber);
    _con.cardNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Add Card", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [



            Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              alignment: Alignment.center,
              child: Form(
                key: _con.formKey,
                child: Column(
                  children: [

                    SizedBox(height: 15,),

                    NeuTextFieldContainer(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      textField: TextFormField(
                        enabled:  !_con.isLoading,
                        validator: (val) => val.isEmpty ? "Enter Valid Card Name" : null,
                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Card Name",
                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                          prefixIcon: Icon(CupertinoIcons.person, color: config.Colors().textColor(1),),
                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                        ),
                        onSaved: (val){
                          _con.payMethod.cardName = val;
                        },
                      ),
                    ),
                    SizedBox(height: 15,),

                    NeuTextFieldContainer(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      textField: TextFormField(
                        enabled:  !_con.isLoading,
                        validator: CardUtils.validateCardNum,
                        // validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(19),
                          new CardNumberInputFormatter()
                        ],
                        controller: _con.cardNumberController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Card Number",
                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                          prefixIcon: CardUtils.getCardIcon(_con.cardType),
                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                        ),
                        onSaved: (val){
                          _con.payMethod.cardNumber = val;
                        },
                      ),
                    ),
                    SizedBox(height: 15,),

                    NeuTextFieldContainer(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      textField: TextFormField(
                        enabled:  !_con.isLoading,
                        validator: CardUtils.validateCVV,
                        // validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(4),
                        ],
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "CVV",
                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                          prefixIcon: Icon(Icons.credit_card, color: config.Colors().textColor(1),),
                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                        ),
                        onSaved: (val){
                          _con.payMethod.cvc = val;
                        },
                      ),
                    ),
                    SizedBox(height: 15,),

                    NeuTextFieldContainer(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      textField: TextFormField(
                        enabled:  !_con.isLoading,
                        validator: CardUtils.validateDate,
                        // validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(4),
                          new CardMonthInputFormatter()
                        ],
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Card Expiry (MM-YY)",
                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                          prefixIcon: Icon(Icons.calendar_today, color: config.Colors().textColor(1),),
                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                        ),
                        onSaved: (val){
                          ///yyyy-MM-dd
                          DateFormat format = DateFormat("MM/yy");
                          DateTime tempDate = format.parse(val);
                          DateFormat frmt = DateFormat("yyyy-MM-dd");
                          _con.payMethod.expiry = frmt.format(tempDate);
                        },
                      ),
                    ),
                    SizedBox(height: 15,),




                  ],
                ),
              ),
            ),


            _con.isLoading ? NeuContainer(
                borderRadius: 100,
                width: 50,
                height: 50,
                child: CircularProgressIndicator()
            )
                : NeuButton(
                  width: 150,
                  height: 50,
                  text: "Save".toUpperCase(),
                  textStyle: textTheme.subtitle2,
                  borderRadius: 100,
                  onPressed: (){
                    _con.saveCard();
                  },
                ),


          ],
        ),
      ),
    );

  }
}
