import 'dart:async';
import 'dart:ui';
import 'package:just_audio/just_audio.dart';
import 'package:lottie/lottie.dart';
import 'package:nurses_app/generated/i18n.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:nurses_app/src/controllers/controller.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/repositories/user_repository.dart' as userRepo;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends StateMVC<SplashScreen> {
  Controller _con;
  final player = AudioPlayer();

  _SplashScreenState() :super(Controller()) {
    _con = controller;
  }

  @override
  void initState() {
    playAudio();
    super.initState();
    loadData();
  }

  playAudio() {
    player.setVolume(1);
    player.setAsset("assets/heart_beat.mp3");
    player.play();
  }

  @override
  void dispose() {
    player?.stop();
    super.dispose();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(milliseconds: 3000), onDoneLoading);
  }

  onDoneLoading() async {
    if (userRepo.currentUser.apiToken == null) {
      Navigator.of(context).pushReplacementNamed('/Login');
    } else {
      if (userRepo.currentUser.verified) {

        if ((userRepo.currentUser.lat != null &&
                userRepo.currentUser.long != null) &&
            (((userRepo.currentUser.lat != 0 ||
                        userRepo.currentUser.lat != "0") &&
                    (userRepo.currentUser.long != 0 ||
                        userRepo.currentUser.long != "0")) ||
                (userRepo.currentUser.lat.length > 0 &&
                    userRepo.currentUser.long.length > 0))) {

          Navigator.of(context).pushNamedAndRemoveUntil(
              "/Pages", (Route<dynamic> route) => false,
              arguments: 2);

        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(
              "/UpdateCurrentLocation", (Route<dynamic> route) => false,
              arguments: RouteArgument(currentUser: userRepo.currentUser));
        }

      } else {
        userRepo.logout().then((value) {
          Navigator.of(context).pushReplacementNamed('/Login');
        });
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      key: _con.scaffoldKey,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [


            Align(
              alignment: Alignment.center,
              child: Hero(
                tag: "Logo",
                  child: Image.asset("assets/img/logo.png", width: 150,)
              ),
            ),
            SizedBox(height: 20,),
            Text(
              S.of(context).appName.toUpperCase(),
              style: textTheme.headline3,
            ),

            Lottie.asset('assets/ecg.json', width: 300),
          ],
        ),
      ),
    );
  }
}
