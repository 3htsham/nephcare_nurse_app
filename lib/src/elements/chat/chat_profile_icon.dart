import 'package:flutter/material.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';

class ChatProfileIcon extends StatelessWidget {

  var link;

  ChatProfileIcon({this.link});

  @override
  Widget build(BuildContext context) {
    return NeuTextFieldContainer(
            padding: EdgeInsets.all(2),
            textField: NeuContainer(
              borderRadius: 100,
              height: 40,
              width: 40,
              child: Padding(
                padding: EdgeInsets.all(1),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: link != null 
                  ? FadeInImage.assetNetwork(
                      placeholder: "assets/img/placeholders/profile_placeholder.png",
                      image: link,
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    ) 
                    : Image.asset(
                        "assets/img/placeholders/profile_placeholder.png",
                        width: 40,
                        height: 40,
                        fit: BoxFit.cover,
                      ),
                ),
              ),
            ),
          );
  }
}