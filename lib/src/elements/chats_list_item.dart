import 'package:nurses_app/src/models/chat.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:intl/intl.dart';


class ChatsListItem extends StatelessWidget {

  Chat chat;
  VoidCallback onTap;
  VoidCallback onLongPress;

  ChatsListItem({this.chat, this.onTap, this.onLongPress});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var dateTime = chat.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return InkWell(
      onTap: this.onTap,
      onLongPress: this.onLongPress,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            NeuContainer(
              height: 70,
              width: 70,
              borderRadius: 100,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: chat.opponent != null && chat.opponent.image !=null && chat.opponent.image.length > 0 
                ? FadeInImage.assetNetwork(
                    placeholder: "assets/img/placeholders/profile_placeholder.png",
                    image: chat.opponent.image,
                  fit: BoxFit.cover,
                  width: 70,
                  height: 70,
                ) 
                : Image.asset("assets/img/placeholders/profile_placeholder.png",
                  fit: BoxFit.cover,
                  width: 70,
                  height: 70,
                )
              ),
            ),
            SizedBox(width: 10,),
            Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(chat.opponent.name,
                    style: textTheme.subtitle2.merge(TextStyle(fontWeight: FontWeight.w600)),
                  ),
                  SizedBox(height: 6,),
                  Container(width: 150,
                      child: Text(
                        chat.lastMessage,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textTheme.caption,)
                  )
                ],
              ),
            ),
            Text(
              day,
              style: textTheme.caption,
            )
          ],
        ),
      ),
    );
  }
}
