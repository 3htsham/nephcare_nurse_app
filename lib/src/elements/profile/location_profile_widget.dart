import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class AddressProfileWidget extends StatelessWidget {

  UserController con;
  VoidCallback onTap;

  AddressProfileWidget({this.con, this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: NeuContainer(
        width: size.width,
        height: 240,
        borderRadius: 10,
        child: Column(
          children: [
            ListTile(
              title: Text("Address", style: textTheme.headline5,),
              leading: GradientIcon(CupertinoIcons.location_solid, size: 22, gradient: config.Colors().iconGradient(),),
              trailing: IconButton(onPressed: this.onTap, icon: Icon(CupertinoIcons.pen, size: 16, color: theme.accentColor,),),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Your address ", style: textTheme.bodyText2,),
                  Container(
                    width: 180,
                    child: Text(
                      con.currentUser != null ? con.currentUser.address ?? "" : "",
                      style: textTheme.caption,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              child: Container(
                height: 130,
                child: GoogleMap(
                    onMapCreated: (GoogleMapController controller) {
                      con.controller = controller;
                      con.controller.setMapStyle(con.mapStyle);
                      con.moveCamera();
                    },
                  mapType: MapType.normal,
                  initialCameraPosition: con.currentUser != null 
                  ? CameraPosition(target: LatLng(con.currentUser.lat != null ?
                      double.parse(con.currentUser.lat.toString()) : 40.7237765,
                      con.currentUser.long != null ?
                      double.parse(con.currentUser.long.toString()) : -74.017617), zoom: 13.0)
                  : CameraPosition(target: LatLng(40.7237765, -74.017617), zoom: 13.0),
                  markers: Set.from(con.allMarkers),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}