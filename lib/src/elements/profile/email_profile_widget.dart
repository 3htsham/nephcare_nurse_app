import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class EmailProfileidget extends StatelessWidget {

  UserController con;

  EmailProfileidget({this.con});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: NeuContainer(
        width: size.width,
        height: 100,
        borderRadius: 10,
        child: Column(
          children: [
            ListTile(
              title: Text("Email", style: textTheme.headline5,),
              leading: GradientIcon(CupertinoIcons.mail_solid, size: 22, gradient: config.Colors().iconGradient(),),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Your email is ", style: textTheme.bodyText2,),
                  Container(
                      width: 100,
                      child: Text(
                        con.currentUser.email ?? "",
                        style: textTheme.caption,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}