import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/QualAndSpecsController.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/qualification/qualification_list_item.dart';

class MyQualificationEditItemsList extends StatelessWidget {

  QualAndSpecsController con;

  MyQualificationEditItemsList({this.con});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: con.qualifications.length,
        itemBuilder: (context, index) {
          return QualificationListItem(
            qualification: con.qualifications[index],
            onRemove: (){
              if(!con.isLoading) {
                con.removeQualification(index);
              }
            },
          );
        }
    );
  }
}