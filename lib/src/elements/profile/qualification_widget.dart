import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class QualificationProfileWidget extends StatelessWidget {

  UserController con;
  VoidCallback onTap;

  QualificationProfileWidget({this.con, this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: NeuContainer(
          width: size.width,
          borderRadius: 10,
          child: Column(
            children: [
              ListTile(
                title: Text("Qualification", style: textTheme.headline5,),
                leading: GradientIcon(CupertinoIcons.news_solid, size: 22, gradient: config.Colors().iconGradient(),),
                trailing: IconButton(onPressed: this.onTap, icon: Icon(CupertinoIcons.pen, size: 16, color: theme.accentColor,),),
              ),
              con.currentUser.qualifications != null && con.currentUser.qualifications.length > 0 
              ? ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  itemCount: con.currentUser.qualifications.length,
                  itemBuilder: (context, index){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(con.currentUser.qualifications[index].degree, 
                              style: textTheme.subtitle2.merge(TextStyle(fontSize: 14)),),
                              Text(con.currentUser.qualifications[index].institute, 
                                style: textTheme.bodyText2.merge(TextStyle(color: theme.accentColor)),),
                            ],
                          ),
                          Container(
                            width: 40,
                            child: Text(
                              con.currentUser.qualifications[index].year,
                              style: textTheme.bodyText2.merge(TextStyle(fontWeight: FontWeight.w700)),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ) : SizedBox(height: 10),

                SizedBox(height: 10)

            ],
          ),
        ),
      );
  }
}