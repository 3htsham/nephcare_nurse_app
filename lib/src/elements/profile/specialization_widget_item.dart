import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/neumorphic/gradient_icon.dart';
import 'package:nurses_app/src/neumorphic/neu_container.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class SpecializationWidgetItem extends StatelessWidget {

  UserController con;
  VoidCallback onTap;

  SpecializationWidgetItem({this.con, this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: NeuContainer(
        width: size.width,
        borderRadius: 10,
        child: Column(
          children: [
            ListTile(
              title: Text("Specialized", style: textTheme.headline5,),
              leading: GradientIcon(CupertinoIcons.checkmark_circle_fill, size: 22, gradient: config.Colors().iconGradient(),),
              trailing: IconButton(onPressed: this.onTap, icon: Icon(CupertinoIcons.pen, size: 16, color: theme.accentColor,),),
            ),
            con.currentUser.specifications != null && con.currentUser.specifications.length > 0
            ? ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: con.currentUser.specifications.length,
              itemBuilder: (context, index){
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 200,
                        child: Text(con.currentUser.specifications[index].specName,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: textTheme.subtitle2.merge(TextStyle(fontSize: 14)),
                        )),
                      Text(
                        con.currentUser.specifications[index].exp + ' year/s',
                        style:textTheme.bodyText2.merge(TextStyle(fontWeight: FontWeight.w700)),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                );
              },
            ) : SizedBox(height: 10),

            SizedBox(height: 10)

            
          ],
        ),
      ),
    );
  }
}