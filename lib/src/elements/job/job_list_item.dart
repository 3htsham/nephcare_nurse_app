import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nurses_app/src/models/job.dart';
import 'package:nurses_app/config/app_config.dart' as config;

class JobListItem extends StatelessWidget {

  Job job;
  VoidCallback onTap;
  VoidCallback onProfileTap;

  JobListItem({@required this.job, @required this.onProfileTap, @required this.onTap});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(job.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);
    
    return InkWell(
        onTap: this.onTap,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(flex: 1, 
                    child: Text(job.title ?? "", 
                    style: textTheme.headline5)
                    ),
                    IconButton(
                      onPressed: this.onTap,
                      icon: Icon(CupertinoIcons.eye, color: theme.accentColor,), 
                    )
                  ],
                ),
                SizedBox(height: 7),
                InkWell(
                  onTap: this.onProfileTap,
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: job.user != null && job.user.image != null && job.user.image.length > 0
                        ? FadeInImage.assetNetwork(
                          placeholder: "assets/img/placeholders/profile_placeholder.png",
                          image: job.user.image,
                          width: 20,
                          height: 20,
                          fit: BoxFit.cover,
                        )
                      : Image.asset(
                          "assets/img/placeholders/profile_placeholder.png",
                          width: 20,
                          height: 20,
                          fit: BoxFit.cover,
                        )
                      ),
                      SizedBox(width: 10),
                      Container(
                        constraints: BoxConstraints(maxWidth: 120),
                        child: Text(job.user.name ?? "Job Poster",
                          style: textTheme.bodyText2,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        )
                      ),
                      SizedBox(width: 10),
                      Text('$day at $time', style: textTheme.caption),
                    ]
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: size.width,
                  child: Text(job.description ?? "",
                  maxLines: 3,
                  overflow: TextOverflow.fade, 
                  style: textTheme.bodyText1),
                ),
                SizedBox(height: 7),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(CupertinoIcons.clock, size: 16, color: config.Colors().textColor(1),),
                        SizedBox(width: 5,),
                        Text('${job.hours ?? 0} hours', style: textTheme.caption,)
                      ],
                    ),
                    SizedBox(width: 25,),
                    Row(
                      children: [
                        Icon(Icons.attach_money, size: 16, color: config.Colors().textColor(1),),
                        SizedBox(width: 5,),
                        Text(job.pay, style: textTheme.caption,)
                      ],
                    ),
                    Spacer(),

                    (job.applied == true || job.applied == "true")
                    ?
                    Row(
                      children: [
                        Icon(CupertinoIcons.checkmark_circle_fill, size: 16, color: theme.accentColor,),
                        SizedBox(width: 5,),
                        Text("Applied", style: textTheme.caption,)
                      ],
                    )
                    : SizedBox(height: 0, width: 0),
                  ],
                ),

                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  height: 0.2, color: theme.focusColor,
                )
              ]
            ),
          )
        ),
      );
  }
}