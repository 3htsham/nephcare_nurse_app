import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:nurses_app/config/strings.dart';

class LocationSearchWidget extends StatelessWidget {

  Function displayPrediction;
  String searchedText;
  LocationSearchWidget({this.displayPrediction, this.searchedText});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Padding(
      padding: const EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
      child: InkWell(
        onTap: () async {
          Prediction p = await PlacesAutocomplete.show(context: context, 
                          apiKey: PLACES_API_KEY, mode: Mode.overlay, 
                          onError: (e){ print(e);});
              
          this.displayPrediction(p);
        },
        child: Container(
          height: 45.0,
          width: double.infinity,
          decoration: BoxDecoration(
              color: theme.scaffoldBackgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              boxShadow: [
                BoxShadow(
                    color: theme.scaffoldBackgroundColor.withOpacity(0.5),
                    blurRadius: 5.0,
                    spreadRadius: 0.0)
              ]),
          child: Padding(
            padding: const EdgeInsets.only(left: 7.0, right: 7.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.search,
                  color: theme.accentColor
                ),
                SizedBox(
                  width: 5.0,
                ),
                Container(
                  width: 270,
                  child: Text(
                    searchedText,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: textTheme.bodyText2
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}