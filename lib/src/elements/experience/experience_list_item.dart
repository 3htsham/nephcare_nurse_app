import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/src/models/specification.dart';

class SpecificationListItem extends StatelessWidget {

  Specification specification;
  VoidCallback onRemove;

  SpecificationListItem({this.specification, this.onRemove});

  @override
  Widget build(BuildContext context) {

    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return ListTile(
      leading: Icon(Icons.stars, color: config.Colors().textColor(1),),
      title: Text(specification.specName, style: textTheme.subtitle2,),
      subtitle: Text("${specification.exp} years", maxLines: 2, overflow: TextOverflow.ellipsis, style: textTheme.bodyText2,),
      trailing: IconButton(
        onPressed: this.onRemove,
        icon: Icon(CupertinoIcons.clear_circled_solid, size: 24, color: theme.accentColor,),
      ),
    );
  }
}