import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/experience/experience_items_list.dart';
import 'package:nurses_app/src/neumorphic/neu_textfield_container.dart';

class ExperienceListContainer extends StatelessWidget {

  UserController con;
  
  ExperienceListContainer({this.con});

  @override
  Widget build(BuildContext context) {
    return NeuTextFieldContainer(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
      borderRadius: BorderRadius.circular(15),
      textField: Container(
        height: 160,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ExperienceItemsList(con: con,),
            ],
          ),
        ),
      ),
    );
  }
}