import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nurses_app/src/controllers/user_controller.dart';
import 'package:nurses_app/src/elements/experience/experience_list_item.dart';

class ExperienceItemsList extends StatelessWidget {

  UserController con;

  ExperienceItemsList({this.con});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: con.specifications.length,
        itemBuilder: (context, index) {
          return SpecificationListItem(
            specification: con.specifications[index], 
            onRemove: (){
                if(!con.isLoading) {
                  con.removeUserExperience(index);
                }
              },
            );
        }
    );
  }
}