import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:nurses_app/config/app_config.dart' as config;
import 'package:nurses_app/generated/i18n.dart';
import 'package:nurses_app/route_generator.dart';
import 'package:nurses_app/src/repositories/settings_repository.dart' as settingsRepo;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("configurations");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
//  /// Supply 'the Controller' for this application.
//  MyApp({Key key}) : super(con: Controller(), key: key);
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return ValueListenableBuilder(
      valueListenable: settingsRepo.locale,
      builder: (context, Locale value, _){
        return MaterialApp(
          title: "Nephcare",
          initialRoute: "/Splash",
          onGenerateRoute: RouteGenerator.generateRoute,
          debugShowCheckedModeBanner: false,
          locale: value,
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
          // localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
          theme:  ThemeData(
            fontFamily: 'Lato',
            primaryColor: config.Colors().scaffoldColor(1),
            brightness: Brightness.light,
            accentColor: config.Colors().mainColor(1),
            focusColor: config.Colors().accentColor(1),
            hintColor: config.Colors().textColor(0.7),
            scaffoldBackgroundColor: config.Colors().scaffoldColor(1),
            textTheme: TextTheme(
              headline1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
              headline2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
              headline3: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
              headline4: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
              headline5: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
              headline6: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
              subtitle1: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
              subtitle2: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
              bodyText1: TextStyle(fontSize: 14.0, color: config.Colors().textColor(1)),
              bodyText2: TextStyle(fontSize: 12.0, color: config.Colors().textColor(1)),
              caption: TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
            ),
          ),
        );
      },
    );

    /*
    return DynamicTheme(
      defaultBrightness: Brightness.dark,
      data: (brightness) {
        // if(brightness == Brightness.dark)
        //   {
        return ThemeData(
          fontFamily: 'Lato',
          primaryColor: config.Colors().scaffoldColor(1),
          brightness: brightness,
          accentColor: config.Colors().mainColor(1),
          focusColor: config.Colors().accentColor(1),
          hintColor: config.Colors().textColor(0.7),
          scaffoldBackgroundColor: config.Colors().scaffoldColor(1),
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
            headline2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
            headline3: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
            headline4: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
            headline5: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
            headline6: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
            subtitle1: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w800, foreground: config.Colors().textForeground()),
            subtitle2: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400, color: config.Colors().textColor(1)),
            bodyText1: TextStyle(fontSize: 14.0, color: config.Colors().textColor(1)),
            bodyText2: TextStyle(fontSize: 12.0, color: config.Colors().textColor(1)),
            caption: TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
          ),
        );
          // }
      },
      themedWidgetBuilder: (context, theme) {
        return ValueListenableBuilder(
          valueListenable: settingsRepo.locale,
          builder: (context, Locale value, _){
            return MaterialApp(
              title: "Nephcare",
              initialRoute: "/Splash",
              onGenerateRoute: RouteGenerator.generateRoute,
              debugShowCheckedModeBanner: false,
              locale: value,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
              theme: theme,
            );
          },
        );
      },
    );
    */
  }
}
