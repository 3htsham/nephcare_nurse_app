import 'package:flutter/material.dart';
import 'package:nurses_app/src/models/route_argument.dart';
import 'package:nurses_app/src/pages/chat/single_chat_widget.dart';
import 'package:nurses_app/src/pages/hire/hire_proposal_details.dart';
import 'package:nurses_app/src/pages/hire/my_hire_proposals.dart';
import 'package:nurses_app/src/pages/hire/ongoing_job_details.dart';
import 'package:nurses_app/src/pages/hire/tracking_location.dart';
import 'package:nurses_app/src/pages/hire/view_ongoing_jobs_list.dart';
import 'package:nurses_app/src/pages/job/view_job.dart';
import 'package:nurses_app/src/pages/login/choose_category.dart';
import 'package:nurses_app/src/pages/login/choose_current_loacation.dart';
import 'package:nurses_app/src/pages/login/email_verification.dart';
import 'package:nurses_app/src/pages/login/login.dart';
import 'package:nurses_app/src/pages/login/qualification.dart';
import 'package:nurses_app/src/pages/login/signup.dart';
import 'package:nurses_app/src/pages/login/specification_widget.dart';
import 'package:nurses_app/src/pages/other/about.dart';
import 'package:nurses_app/src/pages/other/contact.dart';
import 'package:nurses_app/src/pages/pages.dart';
import 'package:nurses_app/src/pages/payment/add_payment_details.dart';
import 'package:nurses_app/src/pages/payment/enter_balance.dart';
import 'package:nurses_app/src/pages/payment/my_wallet.dart';
import 'package:nurses_app/src/pages/profile/edit_profile.dart';
import 'package:nurses_app/src/pages/profile/edit_qualifications.dart';
import 'package:nurses_app/src/pages/profile/edit_specifications.dart';
import 'package:nurses_app/src/pages/profile/update_location.dart';
import 'package:nurses_app/src/pages/profile/view_user_profile.dart';
import 'package:nurses_app/src/pages/splash_screen.dart';

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch(settings.name)
    {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Login':
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => Login(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/ChooseCategory":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ChooseCategoryWidget(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Signup":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SignUp(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Qualification":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => QualificationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Experience":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SpecificationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/UpdateCurrentLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ChooseCurrentLocation(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/ViewJobDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewJobWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/OpenChat":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SingleChatWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditMyLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => UpdateCurrentLocation(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/EditProfile":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditProfile(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditQualifications":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditQualificationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditSpecifications":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditSpecificationsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/VieUserProfile":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewUserProfileWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MyHireRequests":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyHireProposalsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/HireRequestDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => HireProposalDetails(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/trackLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => TrackingLocationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });



      case "/Pages":
        return MaterialPageRoute(builder: (_) => PagesWidget(currentTab: args));


      case "/About":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AboutUsWidget(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/Contact":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ContactUs(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/VerifyEmail":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EmailVerificationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/MyWallet":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyWalletWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/AddPaymentMethod":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AddPaymentMethodWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/AddWalletBalance":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EnterBalanceWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/ViewOnGoingJobsList":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => OngoingJobsListsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ViewOnGoingJobDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => OngoingJobDetailsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
    }
  }

}